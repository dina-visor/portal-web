<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'ApiController@login');
Route::post('restore-password', 'ApiController@restorePassword');
Route::post('refresh-token-notifications', 'ApiController@refreshTokenNotifications');
Route::post('get-branches-api', 'ApiController@getBranchesApi');
Route::post('dashboard-api', 'ApiController@dashboardApi');


Route::post('get-serial-information', 'ApiController@getSerialInformation');
Route::post('save-control', 'ApiController@saveControl');
Route::post('save-turn', 'ApiController@saveTurn');
Route::post('save-transaction', 'ApiController@saveTransaction');
Route::post('revoke-serial', 'ApiController@revokeSerial');