<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::prefix('admin')->group(function () {

    Route::group(['middleware' => ['auth']], function () {
        Route::get('dashboard', 'DashboardController@index');
        Route::resource('clientes', 'ClientController');
        Route::resource('sucursales', 'BranchController');
        Route::resource('usuarios', 'UserController');
        Route::resource('planes', 'PlanController');
        Route::resource('contratos', 'ContractController');
        Route::resource('bitacora', 'LogbookController');

        Route::post('saveClient', 'ClientController@saveClient');
        Route::post('updateClient', 'ClientController@updateClient');
        Route::post('deleteClient', 'ClientController@deleteClient');
        Route::post('searchClient', 'ClientController@searchClient');
        Route::post('getClient', 'ClientController@getClient');
        Route::post('getCities', 'ClientController@getCities');
        Route::post('getNeights', 'ClientController@getNeights');
        Route::post('saveBranch', 'ClientController@saveBranch');
        Route::post('updateBranch', 'ClientController@updateBranch');
        Route::post('saveBrand', 'ClientController@saveBrand');

        Route::post('searchBranch', 'BranchController@searchBranch');
        Route::post('filterBranch', 'BranchController@filterBranch');
        Route::post('getBranch', 'BranchController@getBranch');
        Route::post('getTransactions', 'BranchController@getTransactions');

        Route::post('saveUser', 'UserController@saveUser');
        Route::post('saveEditUser', 'UserController@saveEditUser');
        Route::post('getBranches', 'UserController@getBranches');
        Route::post('getUser', 'UserController@getUser');

        Route::post('savePlan', 'PlanController@store');
        Route::post('updateStatusPlan', 'PlanController@updateStatusPlan');
        Route::post('getPlan', 'PlanController@getPlan');
        Route::post('updatePlan', 'PlanController@updatePlan');

        Route::post('saveContract', 'ContractController@store');
        Route::post('searchContracts', 'ContractController@searchContracts');
        Route::post('updateStatusContract', 'ContractController@updateStatusContract');
        Route::post('findContracts', 'ContractController@findContracts');
        Route::post('saveContracts', 'ContractController@saveContracts');

    });
    
});

Route::get('test/{id}', 'BranchController@test');
Route::get('populate', 'DashboardController@populate');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
