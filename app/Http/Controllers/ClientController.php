<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use App\Brand;
use App\State;
use App\City;
use App\Neight;
use App\Branch;
use App\Transaction;

class ClientController extends Controller
{
    public function index(){
    	$clients = Client::limit(10)->orderBy('id','DESC')->get();
    	$brands = Brand::all();
        $states = State::all();
    	return view('clients.index', ['clients'=>$clients, 'brands'=>$brands, 'states'=>$states]);
    }

    public function saveClient(Request $request){
    	$client = new Client();
    	$client->nombre = $request->name;
    	$client->correo = $request->email;
    	$client->telefono = $request->phone;
    	$client->marca_id = $request->brand;
    	$client->estatus = 1;
    	$client->save();

    	return response()->json($client);
    }

    public function updateClient(Request $request){
        $client = Client::find($request->id);
        $client->nombre = $request->name;
        $client->correo = $request->email;
        $client->telefono = $request->phone;
        $client->marca_id = $request->brand;
        $client->estatus = 1;

        if ($request->hasFile('profile_picture')) {
            $files = glob(public_path().'/img/clients/'.$client->id.'/*'); // get all file names
            foreach($files as $file){ // iterate files
              if(is_file($file))
                unlink($file); // delete file
            }
            $image = $request->file('profile_picture');
            // Rename image
            $filename = time().'.'.$image->guessExtension();
            
            $path = $request->file('profile_picture')->storeAs(
                 'img/clients/'.$client->id, $filename
            );

            $client->logo = 'img/clients/'.$client->id.'/'.$filename;

            $status = "uploaded";            
        }

        $client->save();

        return response()->json($client);
    }

    public function saveBrand(Request $request){
        $brand = new Brand();
        $brand->nombre = $request->name;
        if(!file_exists(public_path().'/img/brands')){
            mkdir(public_path().'/img/brands', 0777);
        }
        if ($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            $filename = time().'.'.$image->guessExtension();
            $path = $request->file('profile_picture')->storeAs(
                 'img/brands/'.$brand->id, $filename
            );
            $brand->logo = 'img/brands/'.$brand->id.'/'.$filename;
            $status = "uploaded";            
        }
        $brand->save();
        return response()->json($brand);
    }

    public function searchClient(Request $request){
    	$clients = Client::where('nombre', 'LIKE', '%'.$request->name.'%')->get();
    	return response()->json($clients);
    }

    public function getClient(Request $request){
        $from_date = date('Y-m-d', strtotime('-2 weeks',strtotime(date('Y-m-d'))));
        $data = [];
        $labels = [];
        $branches = Branch::where('cliente_id', $request->id)->get();
        $n = 0;
        foreach ($branches as $branch) {
            $total_per_day = [];
            for ($i=0; $i < 15; $i++) { 
                $formated_day = date('d/m/Y', strtotime('+'.$i.' day', strtotime($from_date)));
                $total = Transaction::whereRaw('fecha = "'.date('Y-m-d', strtotime('+'.$i.' day', strtotime($from_date))).'" AND estatus = 1 AND sucursal_id = '.$branch->id)->sum('total');
                array_push($total_per_day, $total);
                if($n == 0){                    
                   array_push($labels, $formated_day);                       
                }                                 
            }
            $data[] = array('data'=>$total_per_day,'label'=>$branch->nombre,'borderColor'=>'#'.$this->random_color(),'fill'=>false);
            $n++;
        }
    	$client = Client::find($request->id);
        if($client->logo != NULL){
            $client->logo = asset($client->logo);
        }
        $client->branches;
    	return response()->json(['client'=>$client, 'data'=>$data,'labels'=>$labels,"date"=>$from_date]);
    }

    public function getCities(Request $request){
        $cities = City::where('estado_id', $request->state_id)->get();
        return response()->json($cities);
    }

    public function getNeights(Request $request){
        $neights = Neight::where('ciudad_id', $request->city_id)->get();
        return response()->json($neights);
    }

    public function saveBranch(Request $request){
        $branch = new Branch();
        $branch->cliente_id = $request->client_id;
        $branch->nombre = $request->name;
        $branch->marca_id = $request->brand_id;
        $branch->calle = $request->address;
        $branch->interior = $request->intNumberBranch;
        $branch->exterior = $request->extNumberBranch;
        $branch->telefono = $request->phoneBranch;
        $branch->ciudad_id = $request->cityBranch;
        $branch->colonia = $request->neightBranch;
        $branch->estado_id = $request->stateBranch;
        $branch->conector = $this->generateSerial();
        $branch->cp = $request->zipBranch;
        $branch->coordenadas = $request->coords;
        $branch->estatus = 1;
        $branch->save();

        $branches = Branch::where('cliente_id', $request->client_id)->with('state','city', 'client', 'brand')->get();
        return response()->json($branches);
    }

    public function updateBranch(Request $request){
        $branch = Branch::find($request->id);
        $branch->nombre = $request->name;
        $branch->marca_id = $request->brand_id;
        $branch->calle = $request->address;
        $branch->interior = $request->intNumberBranch;
        $branch->exterior = $request->extNumberBranch;
        $branch->telefono = $request->phoneBranch;
        $branch->ciudad_id = $request->cityBranch;
        $branch->colonia = $request->neightBranch;
        $branch->estado_id = $request->stateBranch;
        $branch->conector = $this->generateSerial();
        $branch->cp = $request->zipBranch;
        $branch->coordenadas = $request->coords;
        $branch->estatus = 1;
        $branch->save();

        $branches = Branch::where('cliente_id', $request->client_id)->with('state','city', 'client', 'brand')->get();
        return response()->json($branches);
    }

    public function deleteClient(Request $request){
        $client = Client::find($request->id);
        foreach ($client->branches as $branch) {
            $branch->delete();
        }
        $client->delete();

        return response()->json(['status'=>1]);
    }
    private function generateSerial(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 60; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    private function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
}
