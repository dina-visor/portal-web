<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contract;
use App\Client;
use App\Plan;

class ContractController extends Controller
{
    public function index(){
    	$contracts = Contract::all();
    	$clients = Client::all();
    	$plans = Plan::all();
    	return view('contracts.index', ['contracts'=>$contracts,'clients'=>$clients,'plans'=>$plans]);
    }

    public function store(Request $request){
    	$contract = new Contract();
    	$contract->fecha = date('Y-m-d');
    	$contract->inicio = date('Y-m-d', strtotime($request->initialDate));
    	$contract->fin = date('Y-m-d', strtotime($request->finalDate));
    	$contract->cliente_id = $request->client;
    	$contract->sucursal_id = $request->branch;
    	$contract->plan_id = $request->plan;
    	$contract->costo = $request->price;
    	$contract->estatus = 1;
    	$contract->save();

    	return response()->json(['status'=>1,'message'=>'Success']);
    }

    public function searchContracts(Request $request){        

        $query = '1';

        if($request->client_id != NULL){
            $query .= ' AND cliente_id = '.$request->client_id; 
        }
        if($request->initial_date != NULL && $request->final_date != NULL){
            $query .= ' AND inicio >= "'.date('Y-m-d', strtotime($request->initial_date)).'"'; 
        }elseif($request->initial_date != NULL && $request->final_date == NULL){
            $query .= ' AND inicio = "'.date('Y-m-d', strtotime($request->initial_date)).'"';
        }
        if($request->final_date != NULL && $request->initial_date != NULL){
            $query .= ' AND fin <= "'.date('Y-m-d', strtotime($request->final_date)).'"'; 
        }elseif($request->final_date != NULL && $request->initial_date == NULL){
            $query .= ' AND fin = "'.date('Y-m-d', strtotime($request->final_date)).'"';
        }

        $contracts = Contract::whereRaw($query)->with('branch', 'client')->get();
        return response()->json(['status'=>1, 'message'=>'success','data'=>$contracts]);
    }

    public function updateStatusContract(Request $request){

        foreach ($request->ids as $id) {
            $contract = Contract::find($id);
            $contract->estatus = $request->status;
            $contract->save();
        }
    }

    public function findContracts(Request $request){
        $contracts = Contract::whereIn('id',$request->ids)->get();
        foreach ($contracts as $contract) {
            $contract->client;
            $contract->branch;
        }
        return response()->json(['data'=>$contracts]);
    }

    public function saveContracts(Request $request){
        foreach ($request->ids as $key => $id) {
            $contract = Contract::find($id);
            $contract->costo = $request->values[$key];
            if($request->plan_id != NULL){
                $contract->plan_id = $request->plan_id;
            }
            $contract->save();
        }
    }
}
