<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Logbook;
use \Carbon\Carbon;

class LogbookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        setlocale(LC_TIME, 'es_ES.UTF-8');
        Carbon::setLocale('es');
        $logs = Logbook::where('fecha', date('Y-m-d'))->with('user', 'category')->get();
        $clients = Client::all();
        return view('logbook.index', ['clients'=>$clients,'logs'=>$logs]);
    }
}
