<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\State;
use App\City;
use App\Client;
use App\Brand;
use App\Branch;
use App\Transaction;
use DateTime;

class BranchController extends Controller
{
    public function index(){
    	$states = State::all();
    	$cities = City::all();
    	$clients = Client::all();
    	$brands = Brand::all();
    	return view('branches.index', ['states'=>$states, 'cities'=>$cities, 'clients'=>$clients, 'brands'=>$brands]);
    }

    public function searchBranch(Request $request){
     $branches = Branch::where('nombre', 'LIKE', '%'.$request->name.'%')->with('state', 'city','brand')->get();
     return response()->json($branches);
    }

    public function filterBranch(Request $request){

        $query = '1';

        if($request->brand != NULL){
            $query .= ' AND marca_id = '.$request->brand;
        }
        if($request->city != NULL){
            $query .= ' AND ciudad_id = '.$request->city;
        }
        if($request->state != NULL){
            $query .= ' AND estado_id = '.$request->state;
        }
        if($request->client != NULL){
            $query .= ' AND cliente_id = '.$request->client;
        }
        if($request->name != NULL){
            $query .= ' AND cliente_id = '.$request->client;
        }


        $branches = Branch::whereRaw($query)->with('state', 'city','brand')->get();
    	return response()->json($branches);
    }

    public function getBranch(Request $request){

    	$branch = Branch::where('id', $request->id)->with('state', 'city','brand', 'client','contract')->first();
        $branch->cities = $branch->state->cities;
        $branch->neights = $branch->city->neights;
        $branch->transactions = Transaction::whereRaw('fecha = "'.date('Y-m-d').'"')->where('sucursal_id', $request->id)->where('estatus',1)->get();
        $branch->last_conection = Transaction::where('sucursal_id', $request->id)->orderBy('id','DESC')->first();
        $labelsDay = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
        $totalDays = [
        Transaction::whereRaw('DAYOFWEEK(fecha) = 1 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total'),
        Transaction::whereRaw('DAYOFWEEK(fecha) = 2 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total'),
        Transaction::whereRaw('DAYOFWEEK(fecha) = 3 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total'),
        Transaction::whereRaw('DAYOFWEEK(fecha) = 4 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total'),
        Transaction::whereRaw('DAYOFWEEK(fecha) = 5 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total'),
        Transaction::whereRaw('DAYOFWEEK(fecha) = 6 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total'),
        Transaction::whereRaw('DAYOFWEEK(fecha) = 7 AND fecha > "'.date("Y-m-d", strtotime("-6 days", strtotime(date("Y-m-d")))).'"')->where('sucursal_id', $request->id)->where('estatus',1)->sum('total')];

        $labelsHours = [];
        $totalHours = [];
        $labelsWeeks = [];
        $totalWeeks = [];

        $ddate = date("Y-m-d");
        $date = new DateTime($ddate);
        $nweek = $date->format("W");
        $branch->cw = intval($nweek);
        $months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Agos", "Sep", "Oct", "Nov", "Dic"];

        $actualMonth = date("m");
        $actualYear = date("Y");
        if(($nweek-3) <= 0){
            for ($i=(($nweek+52)-3); $i <= ($nweek+52); $i++) { 
                if($i > 52){
                   array_push($labelsWeeks, "Sem ".($i - 52));
                   array_push($totalWeeks, Transaction::whereRaw('WEEK(fecha) = '.$i.' AND YEAR(fecha) = '.$actualYear.' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
                }else{
                   array_push($labelsWeeks, "Sem ".$i);
                   array_push($totalWeeks, Transaction::whereRaw('WEEK(fecha) = '.($i-1).' AND YEAR(fecha) = '.$actualYear.' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
                }
            }            
        }else{
            for ($i=$nweek; $i <= 52; $i++) { 
                array_push($labelsWeeks, "Sem ".$i);
                array_push($totalWeeks, Transaction::whereRaw('WEEK(fecha) = '.$i.' AND YEAR(fecha) = '.$actualYear.' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
            }             
        }

        for ($i=0; $i <= 23; $i++) { 
            array_push($labelsHours, date('H:i',strtotime($i.':00')));
            array_push($totalHours, Transaction::whereRaw('HOUR(hora) = '.$i.' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
        }

        for ($i=0; $i <= 23; $i++) { 
            array_push($labelsHours, date('H:i',strtotime($i.':00')));
            array_push($totalHours, Transaction::whereRaw('HOUR(hora) = '.$i.' AND fecha > "'.date("Y-m-d", strtotime("-1 day", strtotime(date("Y-m-d")))).'" AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
        }

        $labelsMonths = [];
        $totalMonths = [];

        if($actualMonth-6 < 0){

            for ($i=(($actualMonth+12)-6); $i < ($actualMonth+12); $i++) { 
                if($i >= 12){

                    array_push($labelsMonths, $months[$i-12]);
                    array_push($totalMonths, Transaction::whereRaw('MONTH(fecha) = '.($i - 12).' AND YEAR(fecha) = '.$actualYear.' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));

                }else{
                    array_push($labelsMonths, $months[$i]);
                    array_push($totalMonths, Transaction::whereRaw('MONTH(fecha) = '.($i).' AND YEAR(fecha) = '.($actualYear-1).' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
                }
                
            }

        }else{

            for ($i=($actualMonth-6); $i < $actualMonth; $i++) { 
                array_push($labelsMonths, $months[$i]);
                array_push($totalMonths, Transaction::whereRaw('MONTH(fecha) = '.$i.' AND YEAR(fecha) = '.$actualYear.' AND estatus = 1 AND sucursal_id = '.$request->id)->sum('total'));
            }

        }

        
        $branch->chartHours = ['labels'=>$labelsHours,'data'=>$totalHours];
        $branch->chartWeeks = ['labels'=>$labelsWeeks,'data'=>$totalWeeks];
        $branch->chartDays = ['labels'=>$labelsDay,'data'=>$totalDays];
        $branch->chartMonths = ['labels'=>$labelsMonths,'data'=>$totalMonths];
        return response()->json($branch);
    }

    public function test($id){
        $from_date = date('Y-m-d', strtotime('-2 weeks',strtotime(date('Y-m-d'))));
        $data = [];
        $labels = [];
        $branches = Branch::where('cliente_id', $id)->get();
        $n = 0;
        foreach ($branches as $branch) {
            $total_per_day = [];
            for ($i=0; $i < 15; $i++) { 
                $formated_day = date('d/m/Y', strtotime('+'.$i.' day', strtotime($from_date)));
                $total = Transaction::whereRaw('fecha = "'.date('Y-m-d', strtotime('+'.$i.' day', strtotime($from_date))).'" AND estatus = 1 AND sucursal_id = '.$branch->id)->sum('total');
                array_push($total_per_day, $total);
                if($n == 0){                    
                   array_push($labels, $formated_day);                       
                }                                 
            }
            $data[] = array('data'=>$total_per_day,'label'=>$branch->nombre,'borderColor'=>'#'.$this->random_color());
            $n++;
        }
        
        return response()->json(['labels'=>$labels,'data'=>$data]);
    }

    public function getTransactions(Request $request){

        $query = '1';
        if($request->initial != NULL && $request->final == NULL){
            $query .= ' AND fecha = "'.date("Y-m-d", strtotime($request->initial)).'"';
        }else if($request->initial != NULL && $request->final != NULL){
            $query .= ' AND fecha >= "'.date("Y-m-d", strtotime($request->initial)).'"';
        }

        if($request->initial == NULL && $request->final != NULL){
            $query .= ' AND fecha = "'.date("Y-m-d", strtotime($request->final)).'"';
        }else if($request->initial != NULL && $request->final != NULL){
            $query .= ' AND fecha <= "'.date("Y-m-d", strtotime($request->final)).'"';
        }
        $transactions = Transaction::whereRaw($query)->where('sucursal_id', $request->id)->get();
        return response()->json($transactions);
    }

}
