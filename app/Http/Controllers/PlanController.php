<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Plan;
use App\UnidadPlan;

class PlanController extends Controller
{
    public function index(){
    	$plans = Plan::all();
    	$unities = UnidadPlan::all();
    	return view('plans.index', ['plans'=>$plans, 'unities'=>$unities]);
    }

    public function store(Request $request){

    	$plan = new Plan();
    	$plan->nombre = $request->name;
    	$plan->descripcion = $request->description;
    	$plan->duracion = $request->duration;
    	$plan->unidad_id = $request->unit;
    	$plan->costo = $request->price;
    	$plan->estatus = $request->status;
    	$plan->save();

    	return response()->json(['status'=>1,'message'=>'success']);

    }

    public function updatePlan(Request $request){
        $plan = Plan::find($request->id);
        $plan->nombre = $request->name;
        $plan->descripcion = $request->description;
        $plan->duracion = $request->duration;
        $plan->unidad_id = $request->unit;
        $plan->costo = $request->price;
        $plan->estatus = $request->status;
        $plan->save();
        return response()->json(['status'=>1,'message'=>'success']);

    }

    public function updateStatusPlan(Request $request){
    	$plan = Plan::find($request->id);
    	$plan->estatus = $request->status;
    	$plan->save();
    	return response()->json(['status'=>1,'message'=>'success']);
    }

    public function getPlan(Request $request){
        $plan = Plan::find($request->id);
        return response()->json(['status'=>1,'message'=>'success','data'=>$plan]);
    }

    public function destroy($id){
        $user = Plan::find($id);
        $user->delete();
        return redirect()->back();
    }
}
