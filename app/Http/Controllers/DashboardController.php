<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use App\Brand;
use App\Branch;
use App\Transaction;
use App\Contract;
use App\Turno;


class DashboardController extends Controller
{
    public function index(){
        $from_date = date('Y-m-d', strtotime('-6 days',strtotime(date('Y-m-d'))));
        $labels = [$from_date];
        $data = [ Transaction::whereRaw('fecha = "'.$from_date.'" AND estatus = 1')->count() ];
        for ($i=1; $i < 7; $i++) { 
            array_push($labels, date('Y-m-d', strtotime('+'.$i.' days',strtotime($from_date))));
            array_push($data, Transaction::whereRaw('fecha = "'.date('Y-m-d', strtotime('+'.$i.' day', strtotime($from_date))).'" AND estatus = 1')->count());
        }
    	$indicators = array(
    	  'clients' => Client::get()->count(),
    	  'branches' => Branch::get()->count(),
    	  'brands' => Brand::get()->count(),
          'labels' => $labels,
          'data' => $data,
          'online_systems' => count(Transaction::select('sucursal_id')->whereRaw('fecha = "'.date('Y-m-d').'" AND hora >= "'.date("H:i:s", strtotime("-2 hours", strtotime(date("H:i:s")))).'"')->groupBy('sucursal_id')->get()),
    	  'transactions' => Transaction::get()->count(),
    	  'contracts' => Contract::whereRaw('fin <= "'.date('Y-m-d', strtotime('+3 weeks', strtotime(date('Y-m-d')))).'"')->get()->count(),
    	);
    	return view('admin.dashboard', ['indicators'=>$indicators]);
    }

    public function populate(){
        /*$date = date('Y-m-d',strtotime("2018-11-01"));
        $turns = Turno::all();
        $ticket = 1;
        $branches = Branch::all();
        for ($i=1; $i < 31 ; $i++) { 
            $date = date('Y-m-d', strtotime($date.'+1 day'));
            foreach ($turns as $turn) {
                for ($x=0; $x <= 23; $x++) { 
                    foreach ($branches as $branch) {

                        $total = rand(100,800);

                        $transaction = new Transaction();
                        $transaction->fecha = date('Y-m-d', strtotime($date));
                        $transaction->hora = date('H:i:s', strtotime($x.":00"));
                        $transaction->turno_id = $turn->id;
                        $transaction->sucursal_id = $branch->id;
                        $transaction->ticket = $ticket;
                        $transaction->total = $total;
                        $transaction->pago = $total;
                        $transaction->estatus = 1;
                        $transaction->persona = 1;
                        $transaction->created_at = date('Y-m-d H:i:s');
                        $transaction->save();
                        $ticket++;
                    }
                }
            }
        }*/
    }
}
