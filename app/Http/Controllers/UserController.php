<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Profile;
use App\Client;
use App\Branch;
use Hash;

class UserController extends Controller
{
    public function index(){
    	$users = User::all();
    	$clients = Client::all();
    	$profiles = Profile::all();
    	return view('users.index', ['users'=>$users, 'clients'=>$clients, 'profiles'=>$profiles]);
    }
    public function create(){}
    public function store(){}
    public function update(){}

    public function destroy($id){
        $user = User::find($id);
        $user->delete();

        return redirect()->back();
    }

    public function saveUser(Request $request){

    	$user = new User();
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = Hash::make($request->password);
    	$user->apellido = $request->last;
    	$user->telefono = $request->phone;
    	$user->cliente_id = $request->client;
    	$user->perfil_id = $request->profile;
    	$user->sucursal_id = $request->branch;
    	$user->save();


    	$status = '';

    	if ($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            // Rename image
            $filename = time().'.'.$image->guessExtension();

            $path = $request->file('profile_picture')->storeAs(
                 'img/users/'.$user->id, $filename
            );

            $user->imagen = 'img/users/'.$user->id.'/'.$filename;

            $status = "uploaded";
        }

        $user->save();

        return $status;

    }

    public function saveEditUser(Request $request){

    	$user = User::find($request->id);
    	$user->name = $request->name;
    	$user->email = $request->email;

    	if($request->password != ''){
    	   $user->password = Hash::make($request->password);
    	}

    	$user->apellido = $request->last;
    	$user->telefono = $request->phone;
    	$user->cliente_id = $request->client;
    	$user->perfil_id = $request->profile;
    	$user->sucursal_id = $request->branch;
    	$user->save();


    	$status = '';

    	if ($request->hasFile('profile_picture')) {
    		$files = glob(public_path().'/img/users/'.$user->id.'/*'); // get all file names
			foreach($files as $file){ // iterate files
			  if(is_file($file))
			    unlink($file); // delete file
			}
            $image = $request->file('profile_picture');
            // Rename image
            $filename = time().'.'.$image->guessExtension();

            $path = $request->file('profile_picture')->storeAs(
                 'img/users/'.$user->id, $filename
            );

            $user->imagen = 'img/users/'.$user->id.'/'.$filename;

            $status = "uploaded";
        }

        $user->save();

        return $status;

    }

    public function getBranches(Request $request){
    	$branches = Branch::where('cliente_id', $request->id)->get();
    	return response()->json($branches);
    }

    public function getUser(Request $request){
    	$user = User::find($request->id);
    	if($user->imagen != '')
    	  $user->imagen = asset($user->imagen);
    	$branches = Branch::where('cliente_id', $user->cliente_id)->get();
    	return response()->json(['data'=>$user, 'branches'=>$branches]);
    }
}
