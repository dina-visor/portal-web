<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
	public $table = 'sucursales';
    public function state(){
    	return $this->belongsTo('App\State', 'estado_id', 'id');
    }

    public function city(){
    	return $this->belongsTo('App\City', 'ciudad_id', 'id');
    }

    public function client(){
    	return $this->belongsTo('App\Client', 'cliente_id', 'id');
    }

    public function brand(){
    	return $this->belongsTo('App\Brand', 'marca_id', 'id');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction', 'sucursal_id', 'id');
    }
    public function contract(){
        return $this->hasOne('App\Contract', 'sucursal_id', 'id');
    }
}
