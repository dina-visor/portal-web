<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'ciudades';

    public function neights(){
    	return $this->hasMany('App\Neight', 'ciudad_id', 'id');
    }
}
