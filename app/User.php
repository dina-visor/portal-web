<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->belongsTo('App\Profile', 'perfil_id', 'id');
    }

    public function client(){
        return $this->belongsTo('App\Client', 'cliente_id', 'id');
    }

    public function branch(){
        return $this->belongsTo('App\Branch', 'sucursal_id', 'id');
    }
}
