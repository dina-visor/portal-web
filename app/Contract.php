<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    public $table = 'contratos';

    public function client(){
    	return $this->hasOne('App\Client', 'id', 'cliente_id');
    }

    public function branch(){
    	return $this->hasOne('App\Branch', 'id', 'sucursal_id');
    }

    public function plan(){
    	return $this->hasOne('App\Plan', 'id', 'plan_id');
    }
}
