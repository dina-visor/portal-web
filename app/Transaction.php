<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $table = 'transacciones';

    public $timestamps = false;

    public function branch(){
    	return $this->hasMany('App\Branch', 'sucursal_id', 'id');
    }
}
