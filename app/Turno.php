<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    public $table = 'turnos';

    public $timestamps = false;

}
