<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $table = 'clientes';

    public function branches(){
    	return $this->hasMany('App\Branch', 'cliente_id', 'id');
    }
}
