<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public $table = 'planes';

    public function unidad(){
    	return $this->hasOne('App\UnidadPlan', 'id', 'unidad_id');
    }
}
