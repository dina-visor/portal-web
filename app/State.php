<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $table = 'estados';

    public function cities(){
    	return $this->hasMany('App\City', 'estado_id', 'id');
    }
}
