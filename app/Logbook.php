<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logbook extends Model
{
    public $table = 'bitacora';

    public function user(){
    	return $this->belongsTo('App\User', 'usuario_id', 'id');
    }

    public function category(){
    	return $this->hasOne('App\LogCategory', 'id', 'categoria');
    }


}
