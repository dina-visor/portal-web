@extends('layouts.admin')

@section('content')
<div class="row">
 <div class="col-sm-12">
   <div class="card">
     <div class="card-body">
      <h2 class="text-center">Planes</h2>
       <table class="table">
         <thead>
           <tr>
            <th></th>
             <th>Nombre</th>
             <th>Descripción</th>
             <th>Duración</th>
             <th>Precio</th>
             <th>Activo</th>
           </tr>
         </thead>
       <tbody>
         @foreach($plans as $plan)
          <tr>
            <td>
              <a class="btn btn-warning editPlan" data-id="{{$plan->id}}" style="float: left; margin-left: 8px;"><i class="icon icon-pencil"></i></a>
              <form action="{{action('PlanController@destroy', $plan->id)}}" method="post">
                @csrf
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" style="float: left; margin-left: 8px;" type="submit"><i class="icon icon-trash"></i></button>
              </form>
            </td>
            <td>{{$plan->nombre}}</td>
            <td>{{$plan->descripcion}}</td>
            <td>{{$plan->duracion}} {{$plan->unidad ? $plan->unidad->nombre:''}}</td>
            <td>${{number_format($plan->costo,2)}}</td>
            <td>
              <input type="checkbox" data-id="{{$plan->id}}" {{$plan->estatus == 1 ? 'checked':''}}>
            </td>
          </tr>
         @endforeach
       </tbody>
     </table>
     <p class="text-center">
       <a class="btn btn-primary" id="newPlan">Nuevo Plan</a>
     </p>
     </div>
   </div>
 </div>

</div>

<div class="modal fade show" id="newPlanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Nuevo Plan</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="namePlan" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Descripción</label>
                    <textarea name="descriptionPlan" id="" cols="30" rows="4" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="">Duración</label>
                    <br>
                    <input type="text" name="durationPlan" class="form-control number text-right" style="width: 48%; float:left; margin:0px 1%;">
                    <select name="unitPlan" id="" class="form-control" style="width: 48%; float:left; margin:0px 1%;">
                      @foreach($unities as $unit)
                        <option value="{{$unit->id}}">{{$unit->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Precio</label>
                    <input type="text" name="pricePlan" class="form-control number">
                  </div>
                  <div class="form-group">
                    <label for="">Activo</label>
                    <input type="checkbox" name="activePlan" checked>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block savePlan">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade show" id="editPlanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Modificar Plan</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="namePlanEdit" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Descripción</label>
                    <textarea name="descriptionPlanEdit" id="" cols="30" rows="4" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="">Duración</label>
                    <br>
                    <input type="text" name="durationPlanEdit" class="form-control number text-right" style="width: 48%; float:left; margin:0px 1%;">
                    <select name="unitPlanEdit" id="" class="form-control" style="width: 48%; float:left; margin:0px 1%;">
                      @foreach($unities as $unit)
                        <option value="{{$unit->id}}">{{$unit->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Precio</label>
                    <input type="text" name="pricePlanEdit" class="form-control number">
                  </div>
                  <div class="form-group">
                    <label for="">Activo</label>
                    <input type="checkbox" name="activePlanEdit" checked>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block updatePlan">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@stop

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
  var c = undefined;
  var cc = undefined;
   

	var	PlansPage = {

	    numArticles: 5,
	    validatorHelper: true,
	    helperMap: false,
	    helperMap2: false,
	    map: undefined,
	    map2: undefined,
	    idClient: 0,
      idEditPlan:0,
      croppie: null,
      el: document.getElementById('resizer'),
	    coords: '19.248479,-103.721080',
	    markers: [],
	    articleList: $("#article-list"),
	    moreButton: $("#more-button"),
	    nameClient: $('input[name="nameClient"]'),
	    phoneClient: $('input[name="phoneClient"]'),
	    emailClient: $('input[name="emailClient"]'),
	    brandClient: $('select[name="brandClient"]'),
	    nameClientEdit: $('.nameClientEdit'),
	    phoneClientEdit: $('.phoneClientEdit'),
	    emailClientEdit: $('.emailClientEdit'),
	    brandClientEdit: $('.brandClientEdit'),
	    modalNewClient: $('#newClientModal'),
	    searchedClient: $('.searchedClient'),
	    listClients: $('.listClients'),
	    listBranches: $('.listBranches'),
	    mapDiv: $('#map'),
	  init: function() {
	  },
    initCroppit: function(event){
      c = $('#resizer').croppie({
                  viewport: {
                      width: 200,
                      height: 200,
                      type: 'circle'
                  },
                  boundary: {
                      width: 250,
                      height: 250
                  },
                  enableOrientation: true
               });
      PlansPage.getImage(event.target, c); 
    },
    initCroppit2: function(event){
      $('.previewImage').hide();
      cc = $('#resizer2').croppie({
                  viewport: {
                      width: 200,
                      height: 200,
                      type: 'circle'
                  },
                  boundary: {
                      width: 250,
                      height: 250
                  },
                  enableOrientation: true
               });
      PlansPage.getImage(event.target, cc); 
    },
    getImage:function(input, croppie){
      if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) { 
                croppie.croppie('bind', {
                    url: e.target.result,
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    },
    rotateImage:function(int){
      croppie.rotate(int);
    },
    base64ImageToBlob: function(str){
      var pos = str.indexOf(';base64,');
      var type = str.substring(5, pos);
      var b64 = str.substr(pos + 8);
    
      // decode base64
      var imageContent = atob(b64);
    
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      var buffer = new ArrayBuffer(imageContent.length);
      var view = new Uint8Array(buffer);
    
      // fill the view, using the decoded base64
      for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
    
      // convert ArrayBuffer to Blob
      var blob = new Blob([buffer], { type: type });
    
      return blob;

    },
    getFinalImage: function(){
      c.croppie('result', 'base64').then(function(base64) {        
      });
    },
	  savePlan: function(){

	  	this.validatorHelper = true;

      $('#newPlanModal .form-control').removeClass('is-invalid');

         if($('input[name="namePlan"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="namePlan"]').addClass('is-invalid');
         }

         if($('textarea[name="descriptionPlan"]').val() == ''){
          this.validatorHelper = false;
          $('textarea[name="descriptionPlan"]').addClass('is-invalid');
         }

         if($('input[name="durationPlan"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="durationPlan"]').addClass('is-invalid');
         }

         if($('input[name="pricePlan"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="pricePlan"]').addClass('is-invalid');
         }

         if(this.validatorHelper == false){ return false; }

        var formData = new FormData();
        formData.append("_token", "{{csrf_token()}}");
        formData.append("name", $('input[name="namePlan"]').val());
        formData.append("description", $('textarea[name="descriptionPlan"]').val());
        formData.append("unit", $('select[name="unitPlan"]').val());
        formData.append("duration", $('input[name="durationPlan"]').val());
        formData.append("price", $('input[name="pricePlan"]').val());
        formData.append("status", $('input[name="activePlan"]').is(':checked') ? '1':'0');
        $.ajax({
          url: "{{url('admin/savePlan')}}",
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){            
            window.location.reload();
          },
          error: function(msg){            
          }

        })
	  },
    updatePlan: function(){

      this.validatorHelper = true;

      $('#editPlanModal .form-control').removeClass('is-invalid');

         if($('input[name="namePlanEdit"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="namePlanEdit"]').addClass('is-invalid');
         }

         if($('textarea[name="descriptionPlanEdit"]').val() == ''){
          this.validatorHelper = false;
          $('textarea[name="descriptionPlanEdit"]').addClass('is-invalid');
         }

         if($('input[name="durationPlanEdit"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="durationPlanEdit"]').addClass('is-invalid');
         }

         if($('input[name="pricePlanEdit"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="pricePlanEdit"]').addClass('is-invalid');
         }

         if(this.validatorHelper == false){ return false; }

        var formData = new FormData();
        formData.append("_token", "{{csrf_token()}}");
        formData.append("id", PlansPage.idEditPlan);
        formData.append("name", $('input[name="namePlanEdit"]').val());
        formData.append("description", $('textarea[name="descriptionPlanEdit"]').val());
        formData.append("unit", $('select[name="unitPlanEdit"]').val());
        formData.append("duration", $('input[name="durationPlanEdit"]').val());
        formData.append("price", $('input[name="pricePlanEdit"]').val());
        formData.append("status", $('input[name="activePlanEdit"]').is(':checked') ? '1':'0');
        $.ajax({
          url: "{{url('admin/updatePlan')}}",
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){            
            window.location.reload();
          },
          error: function(msg){            
          }

        })
    },
    saveEditPlan: function(){

      this.validatorHelper = true;

      $('#editPlanModal .form-control').removeClass('is-invalid');

         if($('input[name="nameEditPlan"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="nameEditPlan"]').addClass('is-invalid');
         }

         if($('input[name="lastEditPlan"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="lastEditPlan"]').addClass('is-invalid');
         }

         if($('input[name="emailEditPlan"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="emailEditPlan"]').addClass('is-invalid');
         }

         if($('select[name="profileEditPlan"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="profileEditPlan"]').addClass('is-invalid');
         }

         if($('select[name="clientEditPlan"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="clientEditPlan"]').addClass('is-invalid');
         }

         if($('select[name="branchEditPlan"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="branchEditPlan"]').addClass('is-invalid');
         }

         if($('input[name="passwordEditPlan"]').val() != '' &&  $('input[name="passwordEditPlan"]').val().length < 7){
          this.validatorHelper = false;
          $('input[name="passwordEditPlan"]').addClass('is-invalid');
          $('input[name="passwordEditPlan"]').after('<span class="is-invalid"> La longitud minima de contraseña: 7 caracteres</span>');
         }

         if($('input[name="passwordEditPlan"]').val() != $('input[name="confirmEditPlan"]').val()){
          this.validatorHelper = false;
          $('input[name="passwordEditPlan"], input[name="confirmEditPlan"]').addClass('is-invalid');
          $('input[name="confirmEditPlan"]').after('<span class="is-invalid"> Las contraseña no coinciden</span>');
         }

         if(this.validatorHelper == false){ return false; }




      cc.croppie('result', 'base64').then(function(base64) {
        var formData = new FormData();
        formData.append("_token", "{{csrf_token()}}");
        formData.append("id", $('input[name="idEditPlan"]').val());
        formData.append("name", $('input[name="nameEditPlan"]').val());
        formData.append("last", $('input[name="lastEditPlan"]').val());
        formData.append("email", $('input[name="emailEditPlan"]').val());
        formData.append("phone", $('input[name="phoneEditPlan"]').val());
        formData.append("profile", $('select[name="profileEditPlan"]').val());
        formData.append("client", $('select[name="clientEditPlan"]').val());
        formData.append("branch", $('select[name="branchEditPlan"]').val());
        formData.append("password", $('input[name="passwordEditPlan"]').val());
        formData.append("profile_picture", PlansPage.base64ImageToBlob(base64));
        $.ajax({
          url: "{{url('admin/saveEditPlan')}}",
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){            
            window.location.reload();
          },
          error: function(msg){            
          }

        })
      });
    },
    editPlan: function(id){
      PlansPage.idEditPlan = id;
      $.ajax({
        url:"{{url('admin/getPlan')}}",
        type:"post",
        async: true,
        data:{
          _token: '{{csrf_token()}}',
          id: id,
        },
        success: function(data){          
          $('select[name="branchEditPlan"]').empty();
          $.each(data.branches, function(index,branch){
               $('select[name="branchEditPlan"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
          $('input[name="idEditPlan"]').val(data.data.id)
          $('input[name="nameEditPlan"]').val(data.data.name)
          $('input[name="lastEditPlan"]').val(data.data.apellido)
          $('input[name="emailEditPlan"]').val(data.data.email)
          $('input[name="phoneEditPlan"]').val(data.data.telefono)
          $('select[name="profileEditPlan"]').val(data.data.perfil_id)
          $('select[name="clientEditPlan"]').val(data.data.cliente_id)
          $('select[name="branchEditPlan"]').val(data.data.sucursal_id)
          if(data.data.imagen == null){
            $('.previewImage').attr('src', 'https://via.placeholder.com/200x200')
          }else{
            $('.previewImage').attr('src', data.data.imagen)
          }
          
          $('#editPlanModal').modal();
        }
      })
    },
    getBranches: function(){
      $.ajax({
        url: "{{url('admin/getBranches')}}",
        type:'post',
        async: true,
        data:{
          _token: "{{csrf_token()}}",
          id: $('select[name="clientPlan"]').val(),
        },
        success: function(data){
          $('select[name="branchPlan"]').empty();
          $.each(data, function(index, branch){
            $('select[name="branchPlan"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
        },
        error: function(msg){          
        }
      })
    },
    getBranchesEdit: function(){
      $.ajax({
        url: "{{url('admin/getBranches')}}",
        type:'post',
        async: true,
        data:{
          _token: "{{csrf_token()}}",
          id: $('select[name="clientEditPlan"]').val(),
        },
        success: function(data){
          $('select[name="branchEditPlan"]').empty();
          $.each(data, function(index, branch){
            $('select[name="branchEditPlan"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
        },
        error: function(msg){          
        }
      })
    },
	  getPlan: function(id){
         var master = this;
         $.ajax({
         		url: '{{url("admin/getPlan")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			id: id,
         		},
         		success: function(data){         			
         			$('input[name="namePlanEdit"]').val(data.data.nombre);
         			$('textarea[name="descriptionPlanEdit"]').val(data.data.descripcion);
         			$('input[name="pricePlanEdit"]').val(data.data.costo);
              $('input[name="durationPlanEdit"]').val(data.data.duracion);
              $('select[name="unitPlanEdit"]').val(data.data.unidad_id);
         			$('input[name="activePlanEdit"]').attr('checked', (data.data.estatus == 0 ? false:true));
         		},
         		error: function(msg){         			
         		}
         	})
	  },
    changeStatusPlan: function(plan,status){
      $.ajax({
            url: '{{url("admin/updateStatusPlan")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              id: plan,
              status: status
            },
            success: function(data){               
            },
            error:function(msg){

            }
      });
    }
	};

	$('.savePlan').click(function(){
		PlansPage.savePlan();
	})
  $('.updatePlan').click(function(){
    PlansPage.updatePlan();
  })
  $('.saveEditPlan').click(function(){
    PlansPage.saveEditPlan();
  })
  $('.editPlan').click(function(){
    $('#editPlanModal').modal();
    PlansPage.idEditPlan = $(this).data('id');
    PlansPage.getPlan($(this).data('id'));
  })
  $('#newPlan').click(function(){
    $('#newPlanModal').modal();
  })
  $(document).on('click', 'table input[type="checkbox"]', function(){
    PlansPage.changeStatusPlan($(this).data('id'), $(this).is(':checked') ? '1':'0');
  })


	$(document).ready(function() {
	    $('table').DataTable();
	    PlansPage.init();
	});
</script>
@stop