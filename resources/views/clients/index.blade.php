@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-sm-12 col-md-3">
		<div class="card">
			<div class="card-body">
				<button class="btn btn-outline-primary btn-sm btn-block" data-toggle="modal" data-target="#newClientModal" type="button">Nuevo Cliente</button>
				<br>
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
						 <input class="form-control searchedClient" id="input2-group2" type="email" name="input2-group2" placeholder="Nombre del cliente">
						 <span class="input-group-append">
						  <button class="btn btn-primary searchClient" type="button"><i class="fa fa-search"></i></button>
						 </span>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<ul class="list-group listClients">
              @foreach($clients as $client)
               <li class="list-group-item listClient" data-id="{{$client->id}}">{{$client->nombre}}</li>
              @endforeach
							<!--<li class="list-group-item">Cras justo odio</li>
							<li class="list-group-item">Dapibus ac facilisis in</li>
							<li class="list-group-item">Morbi leo risus</li>
							<li class="list-group-item">Porta ac consectetur ac</li>
							<li class="list-group-item">Vestibulum at eros</li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-9">
		<div class="card">
			<div class="card-body">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
					 <a class="nav-link active" data-toggle="tab" href="#home3" role="tab" aria-controls="home">
					 <i class="icon-calculator"></i> Datos</a>
					</li>
					<li class="nav-item">
					 <a class="nav-link branchTab" data-toggle="tab" href="#profile3" onClick="ClientsPage.showMap();" role="tab" aria-controls="profile">
					 <i class="icon-basket-loaded"></i> Sucursales</a>
					</li>
					<li class="nav-item">
					 <a class="nav-link" data-toggle="tab" href="#messages3" role="tab" aria-controls="messages">
					 <i class="icon-pie-chart"></i> Trafico</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="home3" role="tabpanel">
						<div class="row">
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="">Nombre</label>
											<input type="text" class="form-control nameClientEdit">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="">Email</label>
											<input type="text" class="form-control emailClientEdit">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="">Teléfono</label>
											<input type="text" class="form-control phoneClientEdit">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="">Marca</label>
											<select class="select form-control brandClientEdit">
												<option value="">Seleccionar Marca</option>
												@foreach($brands as $brand)
												 <option value="{{$brand->id}}">{{$brand->nombre}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
                <h5 class="text-center">Logo de Cliente</h5>
                <img class="no-image" src="https://www.baddorf.com/wp-content/uploads/2010/09/default-image.jpg" style="width: 200px; height: 200px; border-radius: 50%; display: block; margin: 20px auto;" alt="">
                <div class="image-editor">
                  <div id="resizer"></div>
                  <label class="btn btn-primary btn-block btn-file" style="display:none;">
                      Seleccione una Imagen <input type="file" id="file-upload" accept="image/*" style="display: none;">
                  </label>
                </div>
              </div>
              <div class="col-sm-12 text-center">
                <a class="btn btn-primary editClient" style="display:none;">Guardar</a>
                <a class="btn btn-danger deleteClient" style="display:none; color: #fff !important;">Eliminar</a>
              </div>
						</div>
					</div>
					<div class="tab-pane" id="profile3" role="tabpanel">
						<div class="row">
							<div class="col-sm-8">
								<div id="map" style="width:100%; height: 450px;"></div>
							</div>
							<div class="col-sm-4">
								<h5 class="text-center">Sucursales</h5>
								<ul class="list-group listBranches">
								</ul>
								<br>
								<button class="btn btn-primary btn-block newBranch">Nueva Sucursal</button>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="messages3" role="tabpanel">
						<div class="row">
              <div class="col-md-12">
                  <canvas id="brandsChart"></canvas>
              </div>
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade show" id="newClientModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Nuevo Cliente</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="nameClient" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Teléfono</label>
                    <input type="text" name="phoneClient" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Correo</label>
                    <input type="email" name="emailClient" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Marca</label>
                    <select name="brandClient" class="form-control">
                    	@foreach($brands as $brand)
                    	  <option value="{{$brand->id}}">{{$brand->nombre}}</option>
                    	@endforeach
                    </select>
                  </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block saveClient">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade show" id="newBrandModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
       <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Nueva Sucursal</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="nameBrand" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Logo</label>
                    <img class="no-image-brand" src="https://www.baddorf.com/wp-content/uploads/2010/09/default-image.jpg" style="width: 200px; height: 200px; border-radius: 50%; display: block; margin: 20px auto;" alt="">
                    <div class="image-editor-brand">
                      <div id="resizerBrand"></div>
                      <label class="btn btn-primary btn-block btn-file">
                          Seleccione una Imagen <input type="file" id="file-upload-brand" accept="image/*" style="display: none;">
                      </label>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block saveBrand">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade show" id="newBranchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title clientBranch"></h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control nameBranch">
                  </div>
                  <div class="form-group">
                    <label for="">Marca</label>
                    <div class="input-group">
                      <select name="" class="form-control brandBranch">
                        @foreach($brands as $brand)
                          <option value="{{$brand->id}}">{{$brand->nombre}}</option>
                        @endforeach
                      </select>
                      <div class="input-group-append openModalBrand" style="cursor: pointer;">
                        <span class="input-group-text">
                         <i class="icon icon-plus"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Calle</label>
                    <input type="text" name="" class="form-control addressBranch">
                  </div>
                  <div class="form-group">
                    <label for="">Número Interior</label>
                    <input type="text" name="" class="form-control intNumberBranch">
                  </div>
                  <div class="form-group">
                    <label for="">Número Exterior</label>
                    <input type="text" name="" class="form-control extNumberBranch">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
              	<div class="form-group">
                    <label for="">Teléfono</label>
                    <input type="email" name="" class="form-control phoneBranch">
                  </div>
              	<div class="form-group">
                    <label for="">Estado</label>
                    <select name="" class="form-control stateBranch">
                    	<option value="">Selecciona un Estado</option>
                    	@foreach($states as $state)
                    	  <option value="{{$state->id}}">{{$state->nombre}}</option>
                    	@endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Ciudad</label>
                    <select name="" class="form-control cityBranch">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Colonia</label>
                    <select name="" class="form-control neightBranch">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Código Postal</label>
                    <input type="email" name="" class="form-control zipBranch">
                  </div>
              </div>
          </div>
              <div class="row">
              	<div class="col-sm-12">
	                <div id="map2" style="width:100%; height: 200px;"></div>
	                <div style="background: rgba(0,0,0,0.6); width: 200px; height: 60px; position: absolute; z-index: 2000px; left:25px; bottom:5px;">
	                	<p style="color:#fff; margin: 4px !important;"><b>Latitud:</b> <span class="latitude">19.248479</span></p>
	                	<p style="color:#fff; margin: 4px !important;"><b>Longitud:</b> <span class="longitude">-103.721080</span></p>
	                </div>
	              </div>
              </div>
              <br>
              <div class="row">
              	<div class="col-sm-6">
	                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
	              </div>
	              <div class="col-sm-6">
	                  <a class="btn btn-primary btn-block saveBranch">Guardar</a>
	              </div>
              </div>
              </div>
            </div>
        </div>
      </div>

      <div class="modal fade show" id="editBranchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title clientEditBranch"></h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control nameEditBranch">
                  </div>
                  <div class="form-group">
                    <label for="">Marca</label>
                    <div class="input-group">
                      <select name="" class="form-control brandEditBranch">
                        @foreach($brands as $brand)
                          <option value="{{$brand->id}}">{{$brand->nombre}}</option>
                        @endforeach
                      </select>
                      <div class="input-group-append openModalBrand" style="cursor: pointer;">
                        <span class="input-group-text">
                         <i class="icon icon-plus"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Calle</label>
                    <input type="text" name="" class="form-control addressEditBranch">
                  </div>
                  <div class="form-group">
                    <label for="">Número Interior</label>
                    <input type="text" name="" class="form-control intNumberEditBranch">
                  </div>
                  <div class="form-group">
                    <label for="">Número Exterior</label>
                    <input type="text" name="" class="form-control extNumberEditBranch">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Teléfono</label>
                    <input type="email" name="" class="form-control phoneEditBranch">
                  </div>
                <div class="form-group">
                    <label for="">Estado</label>
                    <select name="" class="form-control stateEditBranch">
                      <option value="">Selecciona un Estado</option>
                      @foreach($states as $state)
                        <option value="{{$state->id}}">{{$state->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Ciudad</label>
                    <select name="" class="form-control cityEditBranch">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Colonia</label>
                    <select name="" class="form-control neightEditBranch">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Código Postal</label>
                    <input type="email" name="" class="form-control zipEditBranch">
                  </div>
              </div>
          </div>
              <div class="row">
                <div class="col-sm-12">
                  <div id="map3" style="width:100%; height: 200px;"></div>
                  <div style="background: rgba(0,0,0,0.6); width: 200px; height: 60px; position: absolute; z-index: 2000px; left:25px; bottom:5px;">
                    <p style="color:#fff; margin: 4px !important;"><b>Latitud:</b> <span class="latitude2">19.248479</span></p>
                    <p style="color:#fff; margin: 4px !important;"><b>Longitud:</b> <span class="longitude2">-103.721080</span></p>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-sm-6">
                    <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary btn-block saveEditBranch">Guardar</a>
                </div>
              </div>
              </div>
            </div>
        </div>
      </div>

@stop

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
var c = undefined;
var cx = undefined;


	var	ClientsPage = {

	    numArticles: 5,
	    validatorHelper: true,
	    helperMap: false,
	    helperMap2: false,
      helperMap3: false,
	    map: undefined,
	    map2: undefined,
      map3: undefined,
	    idClient: 0,
      idEditBranch: 0,
      changeImage: false,
      changeImageBrand: false,
	    coords: '19.248479,-103.721080',
      coords2: '19.248479,-103.721080',
	    markers: [],
	    articleList: $("#article-list"),
	    moreButton: $("#more-button"),
	    nameClient: $('input[name="nameClient"]'),
	    phoneClient: $('input[name="phoneClient"]'),
	    emailClient: $('input[name="emailClient"]'),
	    brandClient: $('select[name="brandClient"]'),
	    nameClientEdit: $('.nameClientEdit'),
	    phoneClientEdit: $('.phoneClientEdit'),
	    emailClientEdit: $('.emailClientEdit'),
	    brandClientEdit: $('.brandClientEdit'),
	    modalNewClient: $('#newClientModal'),
	    searchedClient: $('.searchedClient'),
	    listClients: $('.listClients'),
	    listBranches: $('.listBranches'),
	    mapDiv: $('#map'),
	  init: function() {

	  },
    initCroppit: function(event){
      $('.no-image').hide();
      c = $('#resizer').croppie({
                  viewport: {
                      width: 200,
                      height: 200,
                      type: 'circle'
                  },
                  boundary: {
                      width: 250,
                      height: 250
                  },
                  enableOrientation: true
               });
      ClientsPage.changeImage = true;
      ClientsPage.getImage(event.target, c);
    },
    initCroppitBrand: function(event){
      $('.no-image-brand').hide();
      cx = $('#resizerBrand').croppie({
                  viewport: {
                      width: 200,
                      height: 200,
                      type: 'circle'
                  },
                  boundary: {
                      width: 250,
                      height: 250
                  },
                  enableOrientation: true
               });
      ClientsPage.changeImageBrand = true;
      ClientsPage.getImageBrand(event.target, cx);
    },
    getImage:function(input, croppie){
      if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                croppie.croppie('bind', {
                    url: e.target.result,
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    },
    getImageBrand:function(input, croppie){
      if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                croppie.croppie('bind', {
                    url: e.target.result,
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    },
	  saveClient: function(){

	  	this.validatorHelper = true;

	  	 $('#newClientModal .form-control').removeClass('is-invalid');

         if(this.nameClient.val() == ''){
         	this.validatorHelper = false;
         	this.nameClient.addClass('is-invalid');
         }

         if(this.emailClient.val() == ''){
         	this.validatorHelper = false;
         	this.emailClient.addClass('is-invalid');
         }

         if(this.phoneClient.val() == ''){
         	this.validatorHelper = false;
         	this.phoneClient.addClass('is-invalid');
         }

         if(this.brandClient.val() == ''){
         	this.validatorHelper = false;
         	this.brandClient.addClass('is-invalid');
         }

         if(this.validatorHelper == true){
         	var master = this;
         	$.ajax({
         		url: '{{url("admin/saveClient")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			name: this.nameClient.val(),
         			email: this.emailClient.val(),
         			phone: this.phoneClient.val(),
         			brand: this.brandClient.val(),
         		},
         		success: function(data){
         			console.log(data)
         			master.modalNewClient.modal('hide')
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})
         }
	  },
	  searchClient: function(){
	  	var master = this;
         $.ajax({
         		url: '{{url("admin/searchClient")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			name: this.searchedClient.val(),
         		},
         		success: function(data){
         			console.log(data)
         			master.listClients.empty();

         			$.each(data, function(index,client){
         				master.listClients.append('<li class="list-group-item listClient" data-id="'+client.id+'">'+client.nombre+'</li>');
         			})
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})
	  },
	  getClient: function(id){
         var master = this;
         $.ajax({
         		url: '{{url("admin/getClient")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			id: id,
         		},
         		success: function(data){
         			console.log(data)
         			master.nameClientEdit.val(data.client.nombre);
         			master.emailClientEdit.val(data.client.correo);
         			master.phoneClientEdit.val(data.client.telefono);
         			master.brandClientEdit.val(data.client.marca_id);
              if(data.client.logo != null){
                $('.no-image').attr('src', data.client.logo);
              }else{
                $('.no-image').attr('src', "https://www.baddorf.com/wp-content/uploads/2010/09/default-image.jpg");
              }
         			$('.clientBranch').text('Nueva Sucursal de '+data.client.nombre);
              $('.image-editor .btn').show();
              $('.editClient').show();
              $('.deleteClient').show();
         			master.idClient = id;

         			master.listBranches.empty();
                    master.deleteMarkers();
         			master.setMarkers(data.client.branches);

         			$.each(data.client.branches, function(index,Branch){
         				master.listBranches.append('<li class="list-group-item listBranch" data-id="'+Branch.id+'">'+Branch.nombre+'</li>');
         			})

              ClientsPage.buildChart(data.labels, data.data, data.date);
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})
	  },
    buildChart:function(labels, info, date){

      var ctxBrands = document.getElementById('brandsChart').getContext('2d');

      var myLineChart = new Chart(ctxBrands, {
          type: 'line',
          data: {
            labels: labels,
            datasets: info,
          },
          options: {
            title: {
              display: true,
              text: 'Total de Ventas Diarias (desde el día: '+date+')'
            }
          }
        });

    },
    base64ImageToBlob: function(str){
      var pos = str.indexOf(';base64,');
      var type = str.substring(5, pos);
      var b64 = str.substr(pos + 8);

      // decode base64
      var imageContent = atob(b64);

      // create an ArrayBuffer and a view (as unsigned 8-bit)
      var buffer = new ArrayBuffer(imageContent.length);
      var view = new Uint8Array(buffer);

      // fill the view, using the decoded base64
      for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }

      // convert ArrayBuffer to Blob
      var blob = new Blob([buffer], { type: type });

      return blob;

    },
    editClient: function(){

      var master = this;

      if(ClientsPage.changeImage){

        c.croppie('result', 'base64').then(function(base64) {
          var formData = new FormData();
          formData.append("_token", "{{csrf_token()}}");
          formData.append("id", master.idClient);
          formData.append("name", master.nameClientEdit.val());
          formData.append("email", master.emailClientEdit.val());
          formData.append("phone", master.phoneClientEdit.val());
          formData.append("brand", master.brandClientEdit.val());
          formData.append("profile_picture", ClientsPage.base64ImageToBlob(base64));
          $.ajax({
            url: "{{url('admin/updateClient')}}",
            method: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
              console.log(data);
							showToast('Exito','Cliente editado con éxito.');
              //window.location.reload();
            },
            error: function(msg){
              console.log(msg.responseText)
            }

          })
        });

      }else{

        $.ajax({
            url: '{{url("admin/updateClient")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              id: master.idClient,
              name: this.nameClientEdit.val(),
              email: this.emailClientEdit.val(),
              phone: this.phoneClientEdit.val(),
              brand: this.brandClientEdit.val(),
            },
            success: function(data){
              console.log(data)
              //master.modalNewClient.modal('hide')
            },
            error: function(msg){
              console.log(msg)
            }
          })

      }

    },
    saveBrand: function(){

      var master = this;

      if(ClientsPage.changeImageBrand){

        cx.croppie('result', 'base64').then(function(base64) {
          var formData = new FormData();
          formData.append("_token", "{{csrf_token()}}");
          formData.append("name", $('input[name="nameBrand"]').val());
          formData.append("profile_picture", ClientsPage.base64ImageToBlob(base64));
          $.ajax({
            url: "{{url('admin/saveBrand')}}",
            method: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
              console.log(data);
              $('.brandBranch').append('<option value="'+data.id+'">'+data.nombre+'</option>');
              $('.brandBranch').find('option').last().attr('selected',true);
              $('#newBrandModal').modal('hide');
              $('#newBranchModal').modal();
              //window.location.reload();
            },
            error: function(msg){
              console.log(msg.responseText)
            }

          })
        });

      }else{

        $.ajax({
            url: '{{url("admin/saveBrand")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              name: $('input[name="nameBrand"]').val(),
            },
            success: function(data){
              console.log(data)
              $('.brandBranch').append('<option value="'+data.id+'">'+data.nombre+'</option>');
              $('.brandBranch').find('option').last().attr('selected',true);
              $('#newBrandModal').modal('hide');
              $('#newBranchModal').modal();
              //master.modalNewClient.modal('hide')
            },
            error: function(msg){
              console.log(msg)
            }
          })

      }

    },
    deleteClient: function(){

      var master = this;

      $.ajax({
            url: '{{url("admin/deleteClient")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              id: master.idClient,
            },
            success: function(data){
              window.location.reload();
            },
            error: function(msg){
              console.log(msg)
            }
          })

    },
	  showMap: function(){

	  	if(this.helperMap == false){

	  		this.map = new google.maps.Map(document.getElementById('map'), {
	          center: {lat: -34.397, lng: 150.644},
	          zoom: 14,
	          streetViewControl:false,
	          zoomControl:false,
	          mapTypeControl:false,
	          fullscreenControl:false,
	          dragabble: false,
	        });

	        this.helperMap = true;

	  	}else{
          setTimeout(function() {
            ClientsPage.fitBoundsToVisibleMarkers();
          }, 100);
      }
	  },
	  showMap2: function(){

	  	var master = this;

	  	if(this.helperMap2 == false){

	  		this.map2 = new google.maps.Map(document.getElementById('map2'), {
	          center: {lat: 19.248479, lng: -103.721080},
	          zoom: 15,
	          streetViewControl:false,
	          zoomControl:false,
	          mapTypeControl:false,
	          fullscreenControl:false,
	        });

	        this.marker2 = new google.maps.Marker({
			        position: new google.maps.LatLng(19.248479, -103.721080),
			        map: this.map2,
			        draggable: true,
			      });

	        this.marker2.addListener('dragend', function() {
	        	$('.latitude').text(master.marker2.getPosition().lat().toFixed(5))
	        	$('.longitude').text(master.marker2.getPosition().lng().toFixed(5))
	        	master.coords = master.marker2.getPosition().lat()+','+master.marker2.getPosition().lng()
			    master.map2.setCenter(master.marker2.getPosition());
			});

	        this.helperMap2 = true;

	  	}else{
	  		this.map2.setCenter(new google.maps.LatLng(19.248479, -103.721080));
	  		this.marker2.setPosition(new google.maps.LatLng(19.248479, -103.721080))
	  	}
	  },
    showMap3: function(coords){

      var master = this;

      var c3 = coords.split(',');

      if(this.helperMap3 == false){

        this.map3 = new google.maps.Map(document.getElementById('map3'), {
            center: {lat: parseFloat(c3[0]), lng: parseFloat(c3[1])},
            zoom: 15,
            streetViewControl:false,
            zoomControl:false,
            mapTypeControl:false,
            fullscreenControl:false,
          });

          this.marker3 = new google.maps.Marker({
              position: new google.maps.LatLng(parseFloat(c3[0]), parseFloat(c3[1])),
              map: this.map3,
              draggable: true,
            });

          this.marker3.addListener('dragend', function() {
            $('.latitude2').text(master.marker3.getPosition().lat().toFixed(5))
            $('.longitude2').text(master.marker3.getPosition().lng().toFixed(5))
            master.coords2 = master.marker3.getPosition().lat()+','+master.marker3.getPosition().lng()
          master.map3.setCenter(master.marker3.getPosition());
      });

          this.helperMap3 = true;

      }else{
        this.map3.setCenter(new google.maps.LatLng(parseFloat(c3[0]), parseFloat(c3[1])));
        this.marker3.setPosition(new google.maps.LatLng(parseFloat(c3[0]), parseFloat(c3[1])))
      }
    },
	  setMarkers: function(data){

			    var marker, i;

			    this.markers = [];
			    var master = this;

			    if(this.map == undefined){
			    	this.showMap();
			    }
			    var bounds = new google.maps.LatLngBounds();

			    for (i = 0; i < data.length; i++) {
			    console.log(data[i])
			      var coords = data[i].coordenadas.split(',');
			      marker = new google.maps.Marker({
			        position: new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1])),
			        map: this.map,
			        id: data[i].id
			      });

			      bounds.extend(marker.getPosition());
			      this.markers.push(marker);
			      this.map.fitBounds(bounds);
			      this.map.panToBounds(bounds);
			  }


		  },
      fitBoundsToVisibleMarkers: function() {
          var bounds = new google.maps.LatLngBounds();
          for (var i=0; i<this.markers.length; i++) {
              if(this.markers[i].getVisible()) {
                  bounds.extend( this.markers[i].getPosition() );
              }
          }
          this.map.fitBounds(bounds);

      },
		  deleteMarkers: function(){
	         this.clearMarkers();
	         this.markers = [];
		  },
		  clearMarkers: function(){
	         this.setMapOnAll(null);
		  },
		  setMapOnAll: function(map){
	        for (var i = 0; i < this.markers.length; i++) {
	          this.markers[i].setMap(map);
	        }
		  },
		  getCities: function(val){
            master = this;
            $.ajax({
         		url: '{{url("admin/getCities")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			state_id: val,
         		},
         		success: function(data){
         			console.log(data)

         			$('.cityBranch').empty();
         			$('.cityBranch').append('<option value="">Seleccione una ciudad</option>');
         			$.each(data, function(index,city){
         				$('.cityBranch').append('<option value="'+city.id+'">'+city.nombre+'</option>');
         			})
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})

		  },
		  getNeights: function(val){
            master = this;
            $.ajax({
         		url: '{{url("admin/getNeights")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			city_id: val,
         		},
         		success: function(data){
         			console.log(data)

         			$('.neightBranch').empty();
         			$('.neightBranch').append('<option value="">Seleccione una colonia</option>');
         			$.each(data, function(index,neight){
         				$('.neightBranch').append('<option value="'+neight.id+'">'+neight.nombre+'</option>');
         			})
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})

		  },
		  saveBranch: function(){
		  	var master = this;
		  	$.ajax({
		  		url: '{{url("admin/saveBranch")}}',
		  		type: 'post',
		  		data:{
		  			_token: '{{csrf_token()}}',
		  			client_id: master.idClient,
		  			name: $('.nameBranch').val(),
		  			brand_id: $('.brandBranch').val(),
		  			address: $('.addressBranch').val(),
		  			intNumberBranch: $('.intNumberBranch').val(),
		  			extNumberBranch: $('.extNumberBranch').val(),
		  			phoneBranch: $('.phoneBranch').val(),
		  			stateBranch: $('.stateBranch').val(),
		  			cityBranch: $('.cityBranch').val(),
		  			neightBranch: $('.neightBranch option:selected').text(),
		  			zipBranch: $('.zipBranch').val(),
		  			coords: master.coords
		  		},
		  		success: function(data){
                    console.log(data)

                    master.listBranches.empty();
                    master.deleteMarkers();
         			master.setMarkers(data);

              $('#newBranchModal').modal('hide')

         			$.each(data, function(index,branch){
         				master.listBranches.append('<li class="list-group-item listBranch" data-id="'+branch.id+'">'+branch.nombre+'</li>');
         			})

              $()
		  		},
		  		error: function(msg){

		  		}
		  	})
		  },
      updateBranch: function(){
        var master = this;
        $.ajax({
          url: '{{url("admin/updateBranch")}}',
          type: 'post',
          data:{
            _token: '{{csrf_token()}}',
            id: master.idEditBranch,
            name: $('.nameEditBranch').val(),
            brand_id: $('.brandEditBranch').val(),
            address: $('.addressEditBranch').val(),
            intNumberBranch: $('.intNumberEditBranch').val(),
            extNumberBranch: $('.extNumberEditBranch').val(),
            phoneBranch: $('.phoneEditBranch').val(),
            stateBranch: $('.stateEditBranch').val(),
            cityBranch: $('.cityEditBranch').val(),
            neightBranch: $('.neightEditBranch option:selected').text() == null ? 'X':$('.neightEditBranch option:selected').text(),
            zipBranch: $('.zipEditBranch').val(),
            coords: master.coords2
          },
          success: function(data){
              console.log(data)

              $('#editBranchModal').modal('hide')
          },
          error: function(msg){

          }
        })
      },
      editBranch: function(id){
        this.idEditBranch = id;
        var master = this;
         $.ajax({
            url: '{{url("admin/getBranch")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              id: id,
            },
            success: function(data){
              console.log(data)

              $('.nameEditBranch').val(data.nombre)
              $('.addressEditBranch').val(data.calle);
              $('.phoneEditBranch').val(data.telefono);
              $('.intNumberEditBranch').val(data.interior);
              $('.extNumberEditBranch').val(data.exterior);
              $('.cityEditBranch').val(data.city_id);
              $('.brandEditBranch').val(data.marca_id);
              $('.stateEditBranch').val(data.estado_id);
              $('.clientEditBranch').val(data.client.nombre);
              $('.zipEditBranch').val(data.cp);

              $('.cityEditBranch').empty();

              $.each(data.cities, function(index, city){
                 $('.cityEditBranch').append('<option value="'+city.id+'">'+city.nombre+'</option>');
              })

              $('.neightEditBranch').empty();

              $.each(data.neights, function(index, neight){
                 $('.neightEditBranch').append('<option value="'+neight.id+'">'+neight.nombre+'</option>');
              })

              $('.cityEditBranch').val(data.ciudad_id);
              $('.neightEditBranch').find('option[text="'+data.colonia+'"]').val();
              $('#editBranchModal').modal()
              ClientsPage.showMap3(data.coordenadas);
            },
            error: function(msg){
              console.log(msg)
            }
          })
      }

	};

	$('.saveClient').click(function(){
		ClientsPage.saveClient();
	})
  $('.editClient').click(function(){
    ClientsPage.editClient();
  })
	$('.saveBranch').click(function(){
		ClientsPage.saveBranch();
	})
	$('.searchClient').click(function(){
		ClientsPage.searchClient();
	})
  $(document).on('click','.listBranch', function(){
    ClientsPage.editBranch($(this).data('id'));
  })
  $(document).on("change", "#file-upload", function(event) {
    ClientsPage.initCroppit(event);
  });
  $(document).on("change", "#file-upload-brand", function(event) {
    ClientsPage.initCroppitBrand(event);
  });
	$('.stateBranch').on('change', function(){
        if($(this).val() != ''){
        	ClientsPage.getCities($(this).val());
        }
	})
	$('.cityBranch').on('change', function(){
        if($(this).val() != ''){
        	ClientsPage.getNeights($(this).val());
        }
	})
  $('.deleteClient').click(function(){
    ClientsPage.deleteClient();
  })
	$(document).on('click', '.listClient', function(){
		ClientsPage.getClient($(this).data('id'));
	})
  $(document).on('click', '.saveEditBranch', function(){
    ClientsPage.updateBranch();
  })
  $(document).on('click', '.saveBrand', function(){
    ClientsPage.saveBrand();
  })
  $(document).on('click', '.openModalBrand', function(event){
    $('#newBranchModal').modal('hide');
    $('#newBrandModal').modal();
  })
	$(document).on('click', '.newBranch', function(){
    alert('Hola')
		if(ClientsPage.idClient > 0){
			$('#newBranchModal').modal()
			ClientsPage.showMap2();
		}

	})

	$(document).ready(function() {
	    $('table').DataTable();
	    ClientsPage.init();
	});
</script>
@stop
