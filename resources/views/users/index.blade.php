@extends('layouts.admin')

@section('content')
<div class="row">
 <div class="col-sm-12">
   <div class="card">
     <div class="card-body">
       <table class="table">
         <thead>
           <tr>
             <th style="min-width: 120px;"></th>
             <th>Nombre</th>
             <th>Correo</th>
             <th>Perfil</th>
             <th>Cliente</th>
             <th>Sucursal</th>
           </tr>
         </thead>
       <tbody>
         @foreach($users as $user)
          <tr>
            <td>
              <a class="btn btn-warning editUser" style="float: left; margin-left: 8px;" data-id="{{$user->id}}">
                <i class="icon icon-pencil"></i>
              </a>
              <form action="{{action('UserController@destroy', $user->id)}}" method="post">
                @csrf
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" style="float: left; margin-left: 8px;" type="submit"><i class="icon icon-trash"></i></button>
              </form>
            </td>
            <td>{{$user->name.' '.$user->apellido}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->profile->nombre}}</td>
            <td>{{$user->client ? $user->client->nombre:'Sin cliente'}}</td>
            <td>{{$user->branch ? $user->branch->nombre:'Sin sucursal'}}</td>
          </tr>
         @endforeach
       </tbody>
     </table>
     <p class="text-center">
       <a class="btn btn-primary" id="newUser">Nuevo Usuario</a>
     </p>
     </div>
   </div>
 </div>

</div>

<div class="modal fade show" id="newUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Nuevo Usuario</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="nameUser" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Apellido</label>
                    <input type="text" name="lastUser" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Correo</label>
                    <input type="email" name="emailUser" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Perfil</label>
                    <select name="profileUser" id="" class="form-control">
                      <option value="">Selecciona un perfil</option>
                      @foreach($profiles as $profile)
                      <option value="{{$profile->id}}">{{$profile->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Clientes</label>
                    <select name="clientUser" id="" class="form-control">
                      <option value="">Seleccione un cliente</option>
                      @foreach($clients as $client)
                      <option value="{{$client->id}}">{{$client->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Sucursales</label>
                    <select name="branchUser" id="" class="form-control">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Teléfono</label>
                    <input type="text" name="phoneUser" class="form-control number">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Contraseña</label>
                    <input type="password" name="passwordUser" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Confirmar Contraseña</label>
                    <input type="password" name="confirmUser" class="form-control">
                </div>
                <div class="image-editor">
                  <label class="btn btn-primary btn-block btn-file">
                      Seleccione una Imagen <input type="file" id="file-upload" accept="image/*" style="display: none;">
                  </label>
                  <div id="resizer"></div>
                </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block saveUser">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade show" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Modificar Usuario</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="hidden" name="idEditUser">
                    <input type="text" name="nameEditUser" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Apellido</label>
                    <input type="text" name="lastEditUser" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Correo</label>
                    <input type="email" name="emailEditUser" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Perfil</label>
                    <select name="profileEditUser" id="" class="form-control">
                      <option value="">Selecciona un perfil</option>
                      @foreach($profiles as $profile)
                      <option value="{{$profile->id}}">{{$profile->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Clientes</label>
                    <select name="clientEditUser" id="" class="form-control">
                      <option value="">Seleccione un cliente</option>
                      @foreach($clients as $client)
                      <option value="{{$client->id}}">{{$client->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Sucursales</label>
                    <select name="branchEditUser" id="" class="form-control">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Teléfono</label>
                    <input type="text" name="phoneEditUser" class="form-control number">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Contraseña</label>
                    <input type="password" name="passwordEditUser" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Confirmar Contraseña</label>
                    <input type="password" name="confirmEditUser" class="form-control">
                </div>
                <div class="image-editor">
                  <label class="btn btn-primary btn-block btn-file">
                      Seleccione una Imagen <input type="file" id="file-upload2" accept="image/*" style="display: none;">
                  </label>
                  <img src="" class="previewImage" style="width: 200px; height: 200px; display:block; margin:10px auto;" alt="">
                  <div id="resizer2" ></div>
                </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block saveEditUser">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@stop

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
  var c = undefined;
  var cc = undefined;
   

	var	UsersPage = {

	    numArticles: 5,
	    validatorHelper: true,
	    helperMap: false,
	    helperMap2: false,
	    map: undefined,
	    map2: undefined,
	    idClient: 0,
      croppie: null,
      imageUser: false,
      el: document.getElementById('resizer'),
	    coords: '19.248479,-103.721080',
	    markers: [],
	    articleList: $("#article-list"),
	    moreButton: $("#more-button"),
	    nameClient: $('input[name="nameClient"]'),
	    phoneClient: $('input[name="phoneClient"]'),
	    emailClient: $('input[name="emailClient"]'),
	    brandClient: $('select[name="brandClient"]'),
	    nameClientEdit: $('.nameClientEdit'),
	    phoneClientEdit: $('.phoneClientEdit'),
	    emailClientEdit: $('.emailClientEdit'),
	    brandClientEdit: $('.brandClientEdit'),
	    modalNewClient: $('#newClientModal'),
	    searchedClient: $('.searchedClient'),
	    listClients: $('.listClients'),
	    listBranches: $('.listBranches'),
	    mapDiv: $('#map'),
	  init: function() {
	  },
    initCroppit: function(event){
      c = $('#resizer').croppie({
                  viewport: {
                      width: 200,
                      height: 200,
                      type: 'circle'
                  },
                  boundary: {
                      width: 250,
                      height: 250
                  },
                  enableOrientation: true
               });
      UsersPage.getImage(event.target, c); 
    },
    initCroppit2: function(event){
      $('.previewImage').hide();
      UsersPage.imageUser = true;
      cc = $('#resizer2').croppie({
                  viewport: {
                      width: 200,
                      height: 200,
                      type: 'circle'
                  },
                  boundary: {
                      width: 250,
                      height: 250
                  },
                  enableOrientation: true
               });
      UsersPage.getImage(event.target, cc); 
    },
    getImage:function(input, croppie){
      if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) { 
                croppie.croppie('bind', {
                    url: e.target.result,
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    },
    rotateImage:function(int){
      croppie.rotate(int);
    },
    base64ImageToBlob: function(str){
      var pos = str.indexOf(';base64,');
      var type = str.substring(5, pos);
      var b64 = str.substr(pos + 8);
    
      // decode base64
      var imageContent = atob(b64);
    
      // create an ArrayBuffer and a view (as unsigned 8-bit)
      var buffer = new ArrayBuffer(imageContent.length);
      var view = new Uint8Array(buffer);
    
      // fill the view, using the decoded base64
      for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
      }
    
      // convert ArrayBuffer to Blob
      var blob = new Blob([buffer], { type: type });
    
      return blob;

    },
    getFinalImage: function(){
      c.croppie('result', 'base64').then(function(base64) {
        console.log(blob);
      });
    },
	  saveUser: function(){

	  	this.validatorHelper = true;

      $('#newUserModal .form-control').removeClass('is-invalid');

         if($('input[name="nameUser"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="nameUser"]').addClass('is-invalid');
         }

         if($('input[name="lastUser"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="lastUser"]').addClass('is-invalid');
         }

         if($('input[name="emailUser"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="emailUser"]').addClass('is-invalid');
         }

         if($('select[name="profileUser"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="profileUser"]').addClass('is-invalid');
         }

         if($('select[name="clientUser"]').val() == '' && $('select[name="profileUser"]').val() != 1){
          this.validatorHelper = false;
          $('select[name="clientUser"]').addClass('is-invalid');
         }

         if($('select[name="branchUser"]').val() == '' && $('select[name="profileUser"]').val() != 1){
          this.validatorHelper = false;
          $('select[name="branchUser"]').addClass('is-invalid');
         }

         if($('input[name="passwordUser"]').val() == '' ){
          this.validatorHelper = false;
          $('input[name="passwordUser"]').addClass('is-invalid');
         }

         if($('input[name="passwordUser"]').val() != '' &&  $('input[name="passwordUser"]').val().length < 7){
          this.validatorHelper = false;
          $('input[name="passwordUser"]').addClass('is-invalid');
          $('input[name="passwordUser"]').after('<span class="is-invalid"> La longitud minima de contraseña: 7 caracteres</span>');
         }

         if($('input[name="passwordUser"]').val() != $('input[name="confirmUser"]').val()){
          this.validatorHelper = false;
          $('input[name="passwordUser"], input[name="confirmUser"]').addClass('is-invalid');
          $('input[name="confirmUser"]').after('<span class="is-invalid"> Las contraseña no coinciden</span>');
         }

         if(this.validatorHelper == false){ return false; }

         if(UsersPage.imageUser == true){

          c.croppie('result', 'base64').then(function(base64) {
          var formData = new FormData();
          formData.append("_token", "{{csrf_token()}}");
          formData.append("name", $('input[name="nameUser"]').val());
          formData.append("last", $('input[name="lastUser"]').val());
          formData.append("email", $('input[name="emailUser"]').val());
          formData.append("phone", $('input[name="phoneUser"]').val());
          formData.append("profile", $('select[name="profileUser"]').val());
          formData.append("client", $('select[name="clientUser"]').val());
          formData.append("branch", $('select[name="branchUser"]').val());
          formData.append("password", $('input[name="passwordUser"]').val());
          formData.append("profile_picture", UsersPage.base64ImageToBlob(base64));
          $.ajax({
            url: "{{url('admin/saveUser')}}",
            method: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
              console.log(data);
              window.location.reload();
            },
            error: function(msg){
              console.log(msg.responseText)
            }

          })
        });

         }else{

          var formData = new FormData();
          formData.append("_token", "{{csrf_token()}}");
          formData.append("name", $('input[name="nameUser"]').val());
          formData.append("last", $('input[name="lastUser"]').val());
          formData.append("email", $('input[name="emailUser"]').val());
          formData.append("phone", $('input[name="phoneUser"]').val());
          formData.append("profile", $('select[name="profileUser"]').val());
          formData.append("client", $('select[name="clientUser"]').val());
          formData.append("branch", $('select[name="branchUser"]').val());
          formData.append("password", $('input[name="passwordUser"]').val());
          $.ajax({
            url: "{{url('admin/saveUser')}}",
            method: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
              console.log(data);
              window.location.reload();
            },
            error: function(msg){
              console.log(msg.responseText)
            }

          })

         }




      
	  },
    saveEditUser: function(){

      this.validatorHelper = true;

      $('#editUserModal .form-control').removeClass('is-invalid');

         if($('input[name="nameEditUser"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="nameEditUser"]').addClass('is-invalid');
         }

         if($('input[name="lastEditUser"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="lastEditUser"]').addClass('is-invalid');
         }

         if($('input[name="emailEditUser"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="emailEditUser"]').addClass('is-invalid');
         }

         if($('select[name="profileEditUser"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="profileEditUser"]').addClass('is-invalid');
         }

         if($('select[name="clientEditUser"]').val() == '' && $('select[name="profileEditUser"]').val() != 1){
          this.validatorHelper = false;
          $('select[name="clientEditUser"]').addClass('is-invalid');
         }

         if($('select[name="branchEditUser"]').val() == '' && $('select[name="profileEditUser"]').val() != 1){
          this.validatorHelper = false;
          $('select[name="branchEditUser"]').addClass('is-invalid');
         }

         if($('input[name="passwordEditUser"]').val() != '' &&  $('input[name="passwordEditUser"]').val().length < 7){
          this.validatorHelper = false;
          $('input[name="passwordEditUser"]').addClass('is-invalid');
          $('input[name="passwordEditUser"]').after('<span class="is-invalid"> La longitud minima de contraseña: 7 caracteres</span>');
         }

         if($('input[name="passwordEditUser"]').val() != $('input[name="confirmEditUser"]').val()){
          this.validatorHelper = false;
          $('input[name="passwordEditUser"], input[name="confirmEditUser"]').addClass('is-invalid');
          $('input[name="confirmEditUser"]').after('<span class="is-invalid"> Las contraseña no coinciden</span>');
         }

         if(this.validatorHelper == false){ return false; }




      cc.croppie('result', 'base64').then(function(base64) {
        var formData = new FormData();
        formData.append("_token", "{{csrf_token()}}");
        formData.append("id", $('input[name="idEditUser"]').val());
        formData.append("name", $('input[name="nameEditUser"]').val());
        formData.append("last", $('input[name="lastEditUser"]').val());
        formData.append("email", $('input[name="emailEditUser"]').val());
        formData.append("phone", $('input[name="phoneEditUser"]').val());
        formData.append("profile", $('select[name="profileEditUser"]').val());
        formData.append("client", $('select[name="clientEditUser"]').val() == null ? '':$('select[name="clientEditUser"]').val());
        formData.append("branch", $('select[name="branchEditUser"]').val() == null ? '':$('select[name="branchEditUser"]').val());
        formData.append("password", $('input[name="passwordEditUser"]').val());
        formData.append("profile_picture", UsersPage.base64ImageToBlob(base64));
        $.ajax({
          url: "{{url('admin/saveEditUser')}}",
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
            console.log(data);
            window.location.reload();
          },
          error: function(msg){
            console.log(msg.responseText)
          }

        })
      });
    },
    editUser: function(id){
      $.ajax({
        url:"{{url('admin/getUser')}}",
        type:"post",
        async: true,
        data:{
          _token: '{{csrf_token()}}',
          id: id,
        },
        success: function(data){
          console.log(data);
          $('select[name="branchEditUser"]').empty();
          $.each(data.branches, function(index,branch){
               $('select[name="branchEditUser"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
          $('input[name="idEditUser"]').val(data.data.id)
          $('input[name="nameEditUser"]').val(data.data.name)
          $('input[name="lastEditUser"]').val(data.data.apellido)
          $('input[name="emailEditUser"]').val(data.data.email)
          $('input[name="phoneEditUser"]').val(data.data.telefono)
          $('select[name="profileEditUser"]').val(data.data.perfil_id)
          $('select[name="clientEditUser"]').val(data.data.cliente_id)
          $('select[name="branchEditUser"]').val(data.data.sucursal_id)
          if(data.data.imagen == null){
            $('.previewImage').attr('src', 'https://via.placeholder.com/200x200')
          }else{
            $('.previewImage').attr('src', data.data.imagen)
          }
          
          $('#editUserModal').modal();
        }
      })
    },
    getBranches: function(){
      $.ajax({
        url: "{{url('admin/getBranches')}}",
        type:'post',
        async: true,
        data:{
          _token: "{{csrf_token()}}",
          id: $('select[name="clientUser"]').val(),
        },
        success: function(data){
          $('select[name="branchUser"]').empty();
          $.each(data, function(index, branch){
            $('select[name="branchUser"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
        },
        error: function(msg){
          console.log(msg)
        }
      })
    },
    getBranchesEdit: function(){
      $.ajax({
        url: "{{url('admin/getBranches')}}",
        type:'post',
        async: true,
        data:{
          _token: "{{csrf_token()}}",
          id: $('select[name="clientEditUser"]').val(),
        },
        success: function(data){
          $('select[name="branchEditUser"]').empty();
          $.each(data, function(index, branch){
            $('select[name="branchEditUser"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
        },
        error: function(msg){
          console.log(msg)
        }
      })
    },
	  getUser: function(id){
         var master = this;
         $.ajax({
         		url: '{{url("admin/getClient")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			id: id,
         		},
         		success: function(data){
         			console.log(data)
         			master.nameClientEdit.val(data.nombre);
         			master.emailClientEdit.val(data.correo);
         			master.phoneClientEdit.val(data.telefono);
         			master.brandClientEdit.val(data.marca_id);
         			$('.clientBranch').text('Nueva Sucursal de '+data.nombre);
         			master.idClient = id;

         			master.listBranches.empty();
                    master.deleteMarkers();
         			master.setMarkers(data.branches);

         			$.each(data.branches, function(index,Branch){
         				master.listBranches.append('<li class="list-group-item listBranch" data-id="'+Branch.id+'">'+Branch.nombre+'</li>');
         			})
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})
	  }
	};

	$('.saveUser').click(function(){
		UsersPage.saveUser();
	})
  $('.saveEditUser').click(function(){
    UsersPage.saveEditUser();
  })
	$('.searchUser').click(function(){
		UsersPage.searchUser();
	})
	$('.stateBranch').on('change', function(){
        if($(this).val() != ''){
        	UsersPage.getCities($(this).val());
        }		
	})
  $('#newUser').click(function(){
    $('#newUserModal').modal();
  })
	$('.cityBranch').on('change', function(){
        if($(this).val() != ''){
        	UsersPage.getNeights($(this).val());
        }		
	})
	$(document).on('click', '.listClient', function(){
		UsersPage.getClient($(this).data('id'));
	})
  $(document).on("change", "#file-upload", function(event) {
    UsersPage.initCroppit(event);
  });
  $(document).on("change", "#file-upload2", function(event) {
    UsersPage.initCroppit2(event);
  });
  $('#newUserModal').on('hidden.bs.modal', function (e) {
     setTimeout(function() { $('#resizer').croppie('destroy'); }, 100);
  })
  $('#editUserModal').on('hidden.bs.modal', function (e) {
     setTimeout(function() { $('#resizer2').croppie('destroy'); }, 100);
  })
  $(document).on('change', 'select[name="clientUser"]', function(){
       UsersPage.getBranches();
  })
  $(document).on('change', 'select[name="clientEditUser"]', function(){
       UsersPage.getBranchesEdit();
  })
  $(document).on('click', '.editUser', function(){
    UsersPage.editUser($(this).data('id'));
  })
	$(document).on('click', '.newBranch', function(){
		if(UsersPage.idClient > 0){
			$('#newBranchModal').modal()
			UsersPage.showMap2();
		}
		
	})

	$(document).ready(function() {
	    $('table').DataTable();
	    UsersPage.init();
	});
</script>
@stop