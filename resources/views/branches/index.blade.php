@extends('layouts.admin')

@section('content')
<style>
   table {border-collapse:collapse; table-layout:fixed;}
   table td {word-wrap:break-word;}
   </style>
<div class="row">
	<div class="col-sm-3">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="input-group">
						 <input class="form-control searchedBranch" id="input2-group2" type="email" name="input2-group2" placeholder="Nombre de la sucursal">
						 <span class="input-group-append">
						  <button class="btn btn-primary searchBranch" type="button"><i class="fa fa-search"></i></button>
						 </span>
						</div>
					</div>
					<br>
					<br>
					<div class="col-sm-12">
							<div class="form-group">
								<select name="" id="" class="form-control brandFilter">
									<option value="">Seleccione una marca</option>
									@foreach($brands as $brand)
									 <option value="{{$brand->id}}">{{$brand->nombre }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<select name="" id="" class="form-control clientFilter">
									<option value="">Seleccione un cliente</option>
									@foreach($clients as $client)
									 <option value="{{$client->id}}">{{$client->nombre }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<select name="" id="" class="form-control stateFilter">
									<option value="">Seleccione un estado</option>
									@foreach($states as $state)
									 <option value="{{$state->id}}">{{$state->nombre }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<select name="" id="" class="form-control cityFilter">
									<option value="">Seleccione una ciudad</option>
									@foreach($cities as $city)
									 <option value="{{$city->id}}">{{$city->nombre }}</option>
									@endforeach
								</select>
							</div>
						</div>
					<div class="col-sm-12">
						<a class="btn btn-primary btn-block filterBranches">Filtrar</a>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<div class="list-group listBranches">
							<!--<a class="list-group-item list-group-item-action" href="#">Dapibus ac facilisis in</a>
							<a class="list-group-item list-group-item-action" href="#">Morbi leo risus</a>
							<a class="list-group-item list-group-item-action" href="#">Porta ac consectetur ac</a>
							<a class="list-group-item list-group-item-action disabled" href="#">Vestibulum at eros</a>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-9">
		<div class="card">
			<div class="card-body">
				<div id="map" style="width:100%; height: 450px;"></div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade show" id="showBranchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content" >
          <!--<div class="modal-header">
            <h4 class="modal-title">Detalle de Sucursal</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>-->
          <div class="modal-body">
            <div class="row">
	              <div class="col-sm-12">
	              	<h3 class="text-center nameBranch"></h3>
	              	<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
					 <a class="nav-link active" data-toggle="tab" href="#home3" role="tab" aria-controls="home">
					 <i class="icon-calculator"></i> Datos</a>
					</li>
					<li class="nav-item">
					 <a class="nav-link" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile">
					 <i class="icon-basket-loaded"></i> Trafico</a>
					</li>
					<li class="nav-item">
					 <a class="nav-link" data-toggle="tab" href="#messages3" role="tab" aria-controls="messages">
					 <i class="icon-pie-chart"></i> Transacciones</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="home3" role="tabpanel">
						<div class="row">
							<div class="col-sm-7">
								<table class="table">
									<tbody>
										<tr>
											<td>Domicilio</td>
											<td class="addressBranch"></td>
										</tr>
										<tr>
											<td>Teléfono</td>
											<td class="phoneBranch"></td>
										</tr>
										<tr>
											<td>Cliente</td>
											<td class="clientBranch"></td>
										</tr>
										<tr>
											<td>Fecha de Inicio</td>
											<td class="startBranch"></td>
										</tr>
										<tr>
											<td>Fecha de Vencimiento</td>
											<td class="endBranch"></td>
										</tr>
										<tr>
											<td>Num. Serie</td>
											<td class="serialBranch" style="word-wrap: break-word !important;"></td>
										</tr>
									</tbody>
								</table>
								</div>
							<div class="col-sm-5">
								<div id="map2" style="width:100%; height: 200px;"></div>
								<table class="table">
									<tbody>
										<tr>
											<td>Estado:</td>
											<td class="statusBranch"></td>
										</tr>
										<tr>
											<td>Ultima conexión:</td>
											<td class="lastConectionBranch"></td>
										</tr>
										<tr>
											<td>Activo:</td>
											<td class="activeBranch"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="profile3" role="tabpanel">
						<div class="row">
							<div class="col-sm-6">
								<canvas id="graphicByHours"></canvas>
							</div>
							<div class="col-sm-6">
								<canvas id="graphicByWeeks"></canvas>
							</div>
							<div class="col-sm-6">
								<canvas id="graphicByDays"></canvas>
							</div>
							<div class="col-sm-6">
								<canvas id="graphicByMonths"></canvas>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="messages3" role="tabpanel">
						<div class="row">
							<div class="col-sm-6">
								<div class="input-group">
								 <input class="form-control searchedBranchTransaction datepicker" id="input2-group2" type="email" name="input2-group2" placeholder="Fecha de Inicio">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="input-group">
								 <input class="form-control searchedBranchTransaction datepicker2" id="input2-group2" type="email" name="input2-group2" placeholder="Fecha de Termino">
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-12">
								<table class="table transactionsTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>Tickets</th>
											<th>Fecha</th>
											<th>Hora</th>
											<th>Total</th>
											<th>Pago</th>
											<th>Estado</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
          <div class="row">
            <div class="col-sm-12">
              <p class="text-center">
                <a href="#" class="btn btn-secondary" data-dismiss="modal">Cerrar</a>
              </p>
            </div>
          </div>
				</div>
	              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@stop

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>

    var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];

	var branchesPage = {

	    numArticles: 5,
	    validatorHelper: true,
	    map: undefined,
	    map2: undefined,
	    markers: [],
	    idBranch: 0,
	    articleList: $("#article-list"),
	    moreButton: $("#more-button"),
	    nameBranch: $('input[name="nameBranch"]'),
	    phoneBranch: $('input[name="phoneBranch"]'),
	    emailBranch: $('input[name="emailBranch"]'),
	    brandBranch: $('select[name="brandBranch"]'),
	    nameBranchEdit: $('.nameBranchEdit'),
	    phoneBranchEdit: $('.phoneBranchEdit'),
	    emailBranchEdit: $('.emailBranchEdit'),
	    brandBranchEdit: $('.brandBranchEdit'),
	    modalNewBranch: $('#newBranchModal'),
	    searchedBranch: $('.searchedBranch'),
	    listBranches: $('.listBranches'),
	    cityFilter: $('.cityFilter'),
	    clientFilter: $('.clientFilter'),
	    brandFilter: $('.brandFilter'),
	    stateFilter: $('.stateFilter'),
	    mapDiv: $('#map'),
	  init: function() {

	        this.map = new google.maps.Map(document.getElementById('map'), {
	          center: {lat: -34.397, lng: 150.644},
	          zoom: 8
	        });

	        $('.datepicker').datepicker({format: 'mm/dd/yyyy',}).change(dateChanged).on('changeDate', dateChanged);

	        $('.datepicker2').datepicker({format: 'mm/dd/yyyy',}).change(dateChanged).on('changeDate', dateChanged);

	  },
	  initSecond: function(coords) {
            var coords = coords.split(',');
	        this.map2 = new google.maps.Map(document.getElementById('map2'), {
	          center: new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1])),
	          zoom: 14,
	          streetViewControl:false,
	          zoomControl:false,
	          mapTypeControl:false,
	          fullscreenControl:false,
	          dragabble: false,
	        });

	        marker = new google.maps.Marker({
		      position: new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1])),
		      map: this.map2,
		    });

	  },
	  setMarkers: function(data){

		    var marker, i;

		    this.markers = [];
		    var master = this;
		    var bounds = new google.maps.LatLngBounds();

		    for (i = 0; i < data.length; i++) {
		      var coords = data[i].coordenadas.split(',');
		      marker = new google.maps.Marker({
		        position: new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1])),
		        map: this.map,
		        id: data[i].id
		      });

		      bounds.extend(marker.getPosition());
		      this.markers.push(marker);

		      google.maps.event.addListener(marker, 'click', (function(marker, i) {
		        return function() {
		          master.getBranch(marker.id);
		        }
		      })(marker, i));

		      this.map.fitBounds(bounds);

		  }
      showToast('Exito','Lista de sucursales obtenida con éxito.');


	  },
	  deleteMarkers: function(){
         this.clearMarkers();
         this.markers = [];
	  },
	  clearMarkers: function(){
         this.setMapOnAll(null);
	  },
	  setMapOnAll: function(map){
        for (var i = 0; i < this.markers.length; i++) {
          this.markers[i].setMap(map);
        }
	  },
	  searchBranch: function(){
	  	var master = this;
         $.ajax({
         		url: '{{url("admin/searchBranch")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			name: this.searchedBranch.val(),
         		},
         		success: function(data){
         			console.log(data)
         			master.listBranches.empty();
                    master.deleteMarkers();
         			master.setMarkers(data);

         			$.each(data, function(index,Branch){
         				master.listBranches.append('<li class="list-group-item listBranch" data-id="'+Branch.id+'">'+Branch.nombre+'</li>');
         			})

         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})
	  },
	  filterBranches: function(){

	  	var master = this;
         $.ajax({
         		url: '{{url("admin/filterBranch")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			name: this.searchedBranch.val(),
         			brand: this.brandFilter.val(),
         			city: this.cityFilter.val(),
         			state: this.stateFilter.val(),
         			client: this.clientFilter.val(),
         		},
         		success: function(data){
         			console.log(data)
         			master.listBranches.empty();
                    master.deleteMarkers();
         			master.setMarkers(data);

         			$.each(data, function(index,Branch){
         				master.listBranches.append('<li class="list-group-item listBranch" data-id="'+Branch.id+'">'+Branch.nombre+'</li>');
         			})
         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})

	  },
	  getBranch: function(id){
	  	branchesPage.idBranch = id;
	  	$('.transactionsTable tbody').empty();
         var master = this;
         $.ajax({
         		url: '{{url("admin/getBranch")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			id: id,
         		},
         		success: function(data){
         			//console.log(data)
         			$('.nameBranch').text(data.nombre)
         			$('.addressBranch').text(data.calle+' '+data.exterior+'-'+data.interior+'. '+data.city.nombre+', '+data.state.nombre);
         			$('.phoneBranch').text(data.telefono);
         			$('.clientBranch').text(data.client.nombre);
         			$('.startBranch').text(data.inicio);
         			$('.endBranch').text(data.fin);
         			$('.serialBranch').text(data.conector);
         			$('.lastBranch').text(data.ultima);
         			$('.startBranch').text(data.contract != null ? data.contract.inicio:'');
         			$('.endBranch').text(data.contract != null ? data.contract.fin:'');
         			$('.statusBranch').text((data.estatus == 1 ? 'En linea':'Desconectado'));
         			$('.activeBranch').text((data.estatus == 1 ? 'Si':'No'));
         			$('.lastConectionBranch').text((data.last_conection == null ? 'No hay transacciones':data.last_conection.fecha+' '+data.last_conection.hora));
         			$('#showBranchModal').modal();

         			var ctx = document.getElementById("graphicByHours").getContext("2d");
         			var ctx2 = document.getElementById("graphicByWeeks").getContext("2d");
         			var ctx3= document.getElementById("graphicByDays").getContext("2d");
         			var ctx4 = document.getElementById("graphicByMonths").getContext("2d");

         			console.log(data)

         			var data3 = {
				        labels: data.chartDays.labels,
				        datasets: [
				            {
				                label: "Ventas Por Día",
				                fillColor: "rgba(220,220,220,0.2)",
				                strokeColor: "rgba(220,220,220,1)",
				                pointColor: "rgba(220,220,220,1)",
				                pointStrokeColor: "#fff",
				                pointHighlightFill: "#fff",
				                pointHighlightStroke: "rgba(220,220,220,1)",
				                data: data.chartDays.data
				            },
				        ]
				    };

				    var data4 = {
				        labels: data.chartMonths.labels,
				        datasets: [
				            {
				                label: "Ventas Por Mes",
				                fillColor: "rgba(220,220,220,0.2)",
				                strokeColor: "rgba(220,220,220,1)",
				                pointColor: "rgba(220,220,220,1)",
				                pointStrokeColor: "#fff",
				                pointHighlightFill: "#fff",
				                pointHighlightStroke: "rgba(220,220,220,1)",
				                data: data.chartMonths.data
				            },
				        ]
				    };

				    var data2 = {
				        labels: data.chartWeeks.labels,
				        datasets: [
				            {
				                label: "Ventas Por Semana",
				                fillColor: "rgba(220,220,220,0.2)",
				                strokeColor: "rgba(220,220,220,1)",
				                pointColor: "rgba(220,220,220,1)",
				                pointStrokeColor: "#fff",
				                pointHighlightFill: "#fff",
				                pointHighlightStroke: "rgba(220,220,220,1)",
				                data: data.chartWeeks.data
				            },
				        ]
				    };

				    var data1 = {
				        labels: data.chartHours.labels,
				        datasets: [
				            {
				                label: "Ventas Por Hora",
				                fillColor: "rgba(220,220,220,0.2)",
				                strokeColor: "rgba(220,220,220,1)",
				                pointColor: "rgba(220,220,220,1)",
				                pointStrokeColor: "#fff",
				                pointHighlightFill: "#fff",
				                pointHighlightStroke: "rgba(220,220,220,1)",
				                data: data.chartHours.data
				            },
				        ]
				    };


				    var options = { };
				    new Chart(ctx,{type: 'line',data: data1,options: options});
				    new Chart(ctx2,{type: 'line',data: data2,options: options});
				    new Chart(ctx3,{type: 'line',data: data3,options: options});
				    new Chart(ctx4,{type: 'line',data: data4,options: options});


         			$('.transactionsTable tbody').empty();

         			$.each(data.transactions, function(index,row){
		    			$('.transactionsTable tbody').append('<tr><td>'+row.id+'</td><td>'+row.ticket+'</td><td>'+row.fecha+'</td><td>'+row.hora+'</td><td>$'+row.total+'</td><td>$'+row.pago+'</td><td>'+(row.estatus == 0 ? 'Cancelado':'Pagado')+'</td></tr>');
		    		})

         			master.initSecond(data.coordenadas)

         		},
         		error: function(msg){
         			console.log(msg)
         		}
         	})
	  },
	};

	$('.saveBranch').click(function(){
		branchesPage.saveBranch();
	})
	$('.searchBranch').click(function(){
		branchesPage.searchBranch();
	})
    $('.filterBranches').click(function(){
		branchesPage.filterBranches();
	})
	$(document).on('change', '.searchedBranchTransaction', function(){
		alert('Hola')
	})

   	$(document).on('click', '.listBranch', function(){
		branchesPage.getBranch($(this).data('id'));
	})
	$(document).ready(function() {
	    //$('table').DataTable();
	    setTimeout(function() {
	    	branchesPage.init();
	    }, 500);
	});
	function dateChanged(ev) {
		$('.transactionsTable tbody').empty();
	    $.ajax({
	    	url: "{{url('admin/getTransactions')}}",
	    	type: "post",
	    	async: true,
	    	data:{
	    		_token: "{{csrf_token()}}",
	    		id:branchesPage.idBranch,
	    		initial: $('.datepicker').val(),
	    		final: $('.datepicker2').val()
	    	},
	    	success: function(data){
	    		console.log(data)
	    		$.each(data, function(index,row){
	    			$('.transactionsTable tbody').append('<tr><td>'+row.id+'</td><td>'+row.ticket+'</td><td>'+row.fecha+'</td><td>'+row.hora+'</td><td>$'+row.total+'</td><td>$'+row.pago+'</td><td>'+(row.estatus == 0 ? 'Pagado':'Cancelado')+'</td></tr>');
	    		})
	    	},
	    	error: function(msg){
	    		console.log(msg)
	    	}
	    })
	}
</script>
@stop
