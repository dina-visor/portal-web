@extends('layouts.admin')

@section('content')
<div class="row">
 <div class="col-sm-12">
   <div class="card">
     <div class="card-body">
      <h2 class="text-center">Contratos</h2>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="">Cliente</label>
            <select name="" id="" class="form-control searchClient">
              <option value="">Seleccione un cliente</option>
              @foreach($clients as $client)
               <option value="{{$client->id}}">{{$client->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="">Fecha Inicio</label>
            <input type="text" class="form-control datepicker searchInitialDate">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="">Fecha Termino</label>
            <input type="text" class="form-control datepicker2 searchFinalDate">
          </div>
        </div>
      </div>
       <table class="table mainTable">
         <thead>
           <tr>
            <th></th>
             <th>Cliente</th>
             <th>Sucursal</th>
             <th>Fecha Inicio</th>
             <th>Fecha Vencimiento</th>
             <th>Precio</th>
             <th>Activo</th>
           </tr>
         </thead>
       <tbody>
          @foreach($contracts as $contract)
           <tr>
             <th><input type="checkbox" {{$contract->finalizado == 1 ? 'disabled':''}} class="selectedContract" data-id="{{$contract->id}}"></th>
             <td>{{$contract->client ? $contract->client->nombre:''}}</td>
             <td>{{$contract->branch ? $contract->branch->nombre:''}}</td>
             <td>{{date('d/m/Y', strtotime($contract->inicio))}}</td>
             <td>{{date('d/m/Y', strtotime($contract->fin))}}</td>
             <td>${{number_format($contract->costo,2)}}</td>
             <th><input type="checkbox" {{$contract->finalizado == 1 ? 'disabled':''}} class="changeStatusContract" data-id="{{$contract->id}}" {{$contract->estatus == 1 ? 'checked':''}}></th>
           </tr>
          @endforeach
       </tbody>
     </table>
     <p class="text-center">
       <a class="btn btn-primary" id="newContract">Nuevo Contrato</a>
     </p>
      <div class="row row-actions" style="display:none;">
        <div class="col-sm-4">
          <a class="btn btn-primary btn-block renewContracts">Renovar</a>
        </div>
        <div class="col-sm-4">
          <a class="btn btn-primary btn-block">Deshabilitar</a>
        </div>
        <div class="col-sm-4">
          <a class="btn btn-primary btn-block">Finalizar</a>
        </div>
      </div>
     </div>
   </div>
 </div>



</div>


<div class="modal fade show" id="newContractModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Nuevo Contrato</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Cliente</label>
                    <select name="clientContract" id="" class="form-control">
                      <option value="">Eligue un cliente</option>
                      @foreach($clients as $client)
                       <option value="{{$client->id}}">{{$client->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Sucursal</label>
                    <select name="branchContract" id="" class="form-control">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Plan</label>
                    <select name="planContract" id="" class="form-control">
                      <option value="">Selecciona un plan</option>
                      @foreach($plans as $plan)
                       <option value="{{$plan->id}}" data-date-start="{{date('m/d/Y')}}" data-date-end="{{date('m/d/Y', strtotime('+'.$plan->duracion.$plan->unidad->tiempo, strtotime(date('m/d/Y'))))}}" data-price="{{$plan->costo}}">{{$plan->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Fecha Inicio</label>
                    <input type="text" name="initialDateContract" class="form-control datepicker3">
                  </div>   
                  <div class="form-group">
                    <label for="">Fecha Termino</label>
                    <input type="text" name="finalDateContract" class="form-control datepicker4">
                  </div>                  
                  <div class="form-group">
                    <label for="">Precio</label>
                    <input type="text" name="priceContract" class="form-control number">
                  </div>             
                </div>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block saveContract">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade show" id="renovationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
       <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content" style="width: 700px;">
          <div class="modal-header">
            <h4 class="modal-title">Renovar</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <select name="" id="" class="form-control planContract">
                    <option value="">Seleccionar Plan</option>
                    @foreach($plans as $plan)
                     <option data-name="{{$plan->nombre}}" data-description="{{$plan->descripcion}}" data-cost="{{$plan->costo}}" data value="{{$plan->id}}">{{$plan->nombre}}</option>
                    @endforeach
                  </select>
                  <h3 class="text-center namePlan"></h3>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <table class="table contractsTable">
                  <thead>
                    <tr>
                      <th>Cliente</th>
                      <th>Sucursal</th>
                      <th>Fecha Inicio</th>
                      <th>Fecha Vencimiento</th>
                      <th>Precio</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
              <div class="col-sm-6">
                  <a data-dismiss="modal" class="btn btn-secondary btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <a class="btn btn-primary btn-block saveContracts">Guardar</a>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@stop

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
  var c = undefined;
  var cc = undefined;
   

	var	ContractsPage = {

	    numArticles: 5,
	    validatorHelper: true,
	    helperMap: false,
	    helperMap2: false,
	    map: undefined,
	    map2: undefined,
	    idClient: 0,
      croppie: null,
	    markers: [],
	    articleList: $("#article-list"),
	    moreButton: $("#more-button"),
	    nameClient: $('input[name="nameClient"]'),
	    phoneClient: $('input[name="phoneClient"]'),
	    emailClient: $('input[name="emailClient"]'),
	    brandClient: $('select[name="brandClient"]'),
	    nameClientEdit: $('.nameClientEdit'),
	    phoneClientEdit: $('.phoneClientEdit'),
	    emailClientEdit: $('.emailClientEdit'),
	    brandClientEdit: $('.brandClientEdit'),
	    modalNewClient: $('#newClientModal'),
	    searchedClient: $('.searchedClient'),
	    listClients: $('.listClients'),
	    listBranches: $('.listBranches'),
	    mapDiv: $('#map'),
	  init: function() {

      $('.datepicker').datepicker({
          format: 'mm/dd/yyyy',
      });

      $('.datepicker2').datepicker({
          format: 'mm/dd/yyyy',
      });

      $('.datepicker3').datepicker({
          format: 'mm/dd/yyyy',
      });

      $('.datepicker4').datepicker({
          format: 'mm/dd/yyyy',
      });
	  },
	  saveContract: function(){

	  	this.validatorHelper = true;

      $('#newContractModal .form-control').removeClass('is-invalid');

         if($('select[name="clientContract"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="clientContract"]').addClass('is-invalid');
         }

         if($('select[name="branchContract"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="branchContract"]').addClass('is-invalid');
         }

         if($('input[name="initialDateContract"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="initialDateContract"]').addClass('is-invalid');
         }

         if($('input[name="finalDateContract"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="finalDateContract"]').addClass('is-invalid');
         }

         if($('select[name="planContract"]').val() == ''){
          this.validatorHelper = false;
          $('select[name="planContract"]').addClass('is-invalid');
         }

         if($('input[name="priceContract"]').val() == ''){
          this.validatorHelper = false;
          $('input[name="priceContract"]').addClass('is-invalid');
         }

         if(this.validatorHelper == false){ return false; }

        var formData = new FormData();
        formData.append("_token", "{{csrf_token()}}");
        formData.append("client", $('select[name="clientContract"]').val());
        formData.append("branch", $('select[name="branchContract"]').val());
        formData.append("initialDate", $('input[name="initialDateContract"]').val());
        formData.append("finalDate", $('input[name="finalDateContract"]').val());
        formData.append("plan", $('select[name="planContract"]').val());
        formData.append("price", $('input[name="priceContract"]').val());
        $.ajax({
          url: "{{url('admin/saveContract')}}",
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
            window.location.reload();
          },
          error: function(msg){
          }

        })
	  },
    getBranches: function(){
      $.ajax({
        url: "{{url('admin/getBranches')}}",
        type:'post',
        async: true,
        data:{
          _token: "{{csrf_token()}}",
          id: $('select[name="clientContract"]').val(),
        },
        success: function(data){
          $('select[name="branchContract"]').empty();
          $('select[name="branchContract"]').append('<option value="">Seleccione una sucursal</option>');
          $.each(data, function(index, branch){
            $('select[name="branchContract"]').append('<option value="'+branch.id+'">'+branch.nombre+'</option>');
          })
        },
        error: function(msg){
        }
      })
    },
	  getContract: function(id){
         var master = this;
         $.ajax({
         		url: '{{url("admin/getClient")}}',
         		type: 'post',
         		asyn: true,
         		data:{
         			_token: '{{csrf_token()}}',
         			id: id,
         		},
         		success: function(data){
         			master.nameClientEdit.val(data.nombre);
         			master.emailClientEdit.val(data.correo);
         			master.phoneClientEdit.val(data.telefono);
         			master.brandClientEdit.val(data.marca_id);
         			$('.clientBranch').text('Nueva Sucursal de '+data.nombre);
         			master.idClient = id;

         			master.listBranches.empty();
                    master.deleteMarkers();
         			master.setMarkers(data.branches);

         			$.each(data.branches, function(index,Branch){
         				master.listBranches.append('<li class="list-group-item listBranch" data-id="'+Branch.id+'">'+Branch.nombre+'</li>');
         			})
         		},
         		error: function(msg){
         		}
         	})
	  },
    changeStatusContract: function(contract,status){
      $.ajax({
            url: '{{url("admin/updateStatusContract")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              ids: [contract],
              status: status
            },
            success: function(data){
            },
            error:function(msg){

            }
      });
    },
    changeStatusContracts: function( ){
      $.ajax({
            url: '{{url("admin/updateStatusContract")}}',
            type: 'post',
            asyn: true,
            data:{
              _token: '{{csrf_token()}}',
              ids: [contract],
              status: status
            },
            success: function(data){
            },
            error:function(msg){

            }
      });
    },
    searchContracts: function(){
      table.clear().draw();
      $.ajax({
        url: '{{url("admin/searchContracts")}}',
        type: 'post',
        asyn: true,
        data:{
          _token: '{{csrf_token()}}',
          client_id: $('.searchClient').val(),
          initial_date:$('.searchInitialDate').val(),
          final_date:$('.searchFinalDate').val(),
        },
        success: function(data){
          console.log(data);
          for (var i = 0; i < data.data.length; i++) {
            table.row.add(['<input type="checkbox" class="selectedContract" data-id="'+data.data[i].id+'">', data.data[i].client.nombre, data.data[i].branch.nombre, convertDate(data.data[i].inicio), convertDate(data.data[i].fin), numeral(data.data[i].costo).format('$0,0.00'), '<input type="checkbox" '+(data.data[i].estatus == 1 ? "checked":"")+' data-id="'+data.data[i].id+'">']);
            if(i == (data.data.length -1)){
              table.draw();
            }
          };
        }
      })
    },
    showContracts: function(){
      var tokens = [];
      $.each($('.selectedContract:checked'), function(index, row){
          tokens.push($(row).data('id'));
      })
        
      console.log(tokens);
      $.ajax({
        url: '{{url("admin/findContracts")}}',
        type: 'post',
        asyn: true,
        data:{
          _token: '{{csrf_token()}}',
          ids: tokens,
        },
        success: function(data){
          console.log(data);
          $('.contractsTable tbody').empty();
          for (var i = 0; i < data.data.length; i++) {
            $('.contractsTable tbody').append('<tr>'+
                                              '<td>'+data.data[i].client.nombre+'</td>'+
                                              '<td>'+data.data[i].branch.nombre+'</td>'+
                                              '<td>'+data.data[i].inicio+'</td>'+
                                              '<td>'+data.data[i].fin+'</td>'+
                                              '<td><input type="text" class="form-control costContract" data-id="'+data.data[i].id+'" value="'+data.data[i].costo+'"/></td>'+
                                              '</tr>');
          };
        }
      })
      $('#renovationModal').modal();
    }
	};

  function convertDate(inputFormat) {
    var xd = inputFormat.split('-');
    return xd[2]+'/'+xd[1]+'/'+xd[0];
  }

	$('.saveContract').click(function(){
		ContractsPage.saveContract();
	})
  $('.saveEditContract').click(function(){
    ContractsPage.saveEditContract();
  })
  $('#newContract').click(function(){
    $('#newContractModal').modal();
  })
  $(document).on('click', '.changeStatusContract', function(){
    ContractsPage.changeStatusContract($(this).data('id'), $(this).is(':checked') ? '1':'0');
  })

  $(document).on('click', '.selectedContract', function(){

    var check = $('.selectedContract:checked').length;
    if(check > 0){
      $('.row-actions').fadeIn('slow');
    }else{
      $('.row-actions').fadeOut('slow');
    }
  })

  $(document).on('change', 'select[name="clientContract"]', function(){
    ContractsPage.getBranches();
  })

  $(document).on('change', '.searchClient', function(){
    ContractsPage.searchContracts();
  })

  $(document).on('click', '.renewContracts', function(){
    ContractsPage.showContracts();
  })

  $(document).on('change', '.planContract', function(){
    if($(this).val() != ''){
      $('.namePlan').text($(this).find('option:selected').data('name')+', '+$(this).find('option:selected').data('description')+'. $'+$(this).find('option:selected').data('cost'))
      $('.costContract').val($(this).find('option:selected').data('cost'))
    }else{
      $('.namePlan').text('')
    }
  })

  $(document).on('click', '.saveContracts', function(){
    var ids = [];
    var values = [];
    for (var i = 0; i < $('.costContract').length; i++) {
      ids.push($('.costContract').eq(i).data('id'))
      values.push($('.costContract').eq(i).val())
    };

    $.ajax({
        url:'{{url("admin/saveContracts")}}',
        type:'post',
        asyn: true,
        data:{
          _token: '{{csrf_token()}}',
          ids: ids,
          values: values,
          plan_id: $('.planContract').val(),
        },
        success: function(data){
          $('#renovationModal').modal('hide');
          $('.contractsTable tbody').empty()
        },
        error:function(msg){

        }

      })
  })

  $('.searchInitialDate, .searchFinalDate').change(function () {
      ContractsPage.searchContracts();
  });

  $(document).on('change', 'select[name="planContract"]', function(){
    $('input[name="priceContract"]').val($('select[name="planContract"] option:selected').data('price'));
    $('.datepicker3').val($('select[name="planContract"] option:selected').data('date-start'));
    $('.datepicker4').val($('select[name="planContract"] option:selected').data('date-end'));
  })

  var table = undefined;

  


	$(document).ready(function() {
	    table = $('.mainTable').DataTable({
        "searching": false,
        "bPaginate": false,
        "bLengthChange": false,
      });
	    ContractsPage.init();
	});
</script>
@stop