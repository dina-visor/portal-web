@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-3">
					  <input class="form-control searchedBranch" id="input2-group2" type="email" name="input2-group2" placeholder="Nombre de la sucursal">
					</div>
					<div class="col-sm-3">
					  <input class="form-control searchedBranch" id="input2-group2" type="email" name="input2-group2" placeholder="Nombre de la sucursal">
					</div>
					<div class="col-sm-3">
					  <div class="form-group">
							<select name="" id="" class="form-control clientFilter">
								<option value="">Seleccione un cliente</option>
								@foreach($clients as $client)
								 <option value="{{$client->id}}">{{$client->nombre }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<a class="btn btn-primary btn-block filterBranches">Filtrar</a>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						@foreach($logs as $log)
						<div class="card">
						  <div class="card-header">
						  	<span>Modificaciones</span>
						  	<span class="pull-right">{{date('l jS \of F Y h:i:s A', strtotime($log->fecha))}}</span>
						  </div>
						  <div class="card-body">
						  	<p>{{$log->user == NULL ? 'Se ':'El usuario '}}<b>{{$log->user == NULL ? '':$log->user->name}}</b> {{$log->category->nombre}}</p>
						  	{{$log->descripcion}}
						  </div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


    </div>

@stop

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
</script>
@stop